/*    */ package com.compani.shooter.main;
/*    */ 
/*    */ public class Constants
/*    */ {
/*    */   public static final float DIMMING_RATE = 0.005F;
/*    */   public static final int PPM = 64;
/*    */   public static final float MPP = 0.015625F;
/* 12 */   public static int WIDTH = 1216;
/* 13 */   public static int HEIGHT = 768;
/*    */   public static final float SCALE = 1.0F;
/*    */   public static final float MAX_VELOCITY = 4.0F;
/* 24 */   public static int[] player1keys = { 32, 29, 51, 47 };
/*    */ 
/*    */   public class keys
/*    */   {
/*    */     public static final int MOVE_FORWARD = 0;
/*    */     public static final int MOVE_BACKWARD = 1;
/*    */     public static final int JUMP = 2;
/*    */     public static final int DUCK = 3;
/*    */ 
/*    */     public keys()
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.main.Constants
 * JD-Core Version:    0.6.2
 */