/*    */ package com.compani.shooter.main;
/*    */ 
/*    */ import com.badlogic.gdx.Application;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.compani.shooter.screens.SplashScreen;
/*    */ 
/*    */ public class Main extends Game
/*    */ {
/*    */   SpriteBatch batch;
/*    */   Texture img;
/*    */ 
/*    */   public void create()
/*    */   {
/* 20 */     Gdx.app.log("Game", "Game Started");
/*    */ 
/* 23 */     setScreen(new SplashScreen(this));
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.main.Main
 * JD-Core Version:    0.6.2
 */