/*    */ package com.compani.shooter.screens;
/*    */ 
/*    */ import com.badlogic.gdx.Application;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.Screen;
/*    */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Vector3;
/*    */ import com.badlogic.gdx.utils.viewport.StretchViewport;
/*    */ import com.badlogic.gdx.utils.viewport.Viewport;
/*    */ import com.compani.shooter.main.Constants;
/*    */ 
/*    */ public class SplashScreen
/*    */   implements Screen
/*    */ {
/*    */   OrthographicCamera cam;
/*    */   Viewport viewPort;
/*    */   private SpriteBatch batch;
/*    */   private Texture splash;
/*    */   double time;
/*    */   Game game;
/*    */ 
/*    */   public SplashScreen(Game g)
/*    */   {
/* 32 */     this.game = g;
/* 33 */     this.batch = new SpriteBatch();
/* 34 */     this.splash = new Texture("splashscreen.jpg");
/*    */ 
/* 36 */     this.cam = new OrthographicCamera();
/* 37 */     this.viewPort = new StretchViewport(Constants.WIDTH, Constants.HEIGHT, this.cam);
/* 38 */     this.cam.position.set(this.viewPort.getWorldWidth() / 2.0F, this.viewPort.getWorldHeight() / 2.0F, 0.0F);
/* 39 */     this.cam.update();
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 46 */     Gdx.app.log("Splash", "Splashcreen loaded");
/* 47 */     this.time = System.currentTimeMillis();
/*    */   }
/*    */ 
/*    */   public void render(float delta)
/*    */   {
/* 53 */     this.batch.setProjectionMatrix(this.cam.combined);
/*    */ 
/* 55 */     if (System.currentTimeMillis() - this.time < 2000.0D) {
/* 56 */       this.batch.begin();
/* 57 */       this.batch.setColor(this.batch.getColor());
/* 58 */       this.batch.draw(this.splash, 0.0F, 0.0F, Constants.WIDTH * 1.0F, Constants.HEIGHT * 1.0F);
/* 59 */       this.batch.end();
/*    */     }
/*    */     else {
/* 62 */       Gdx.app.log("Splash", "Splashcreen Ends");
/* 63 */       dispose();
/* 64 */       this.game.setScreen(new MainMenuScreen(this.game));
/*    */     }
/*    */   }
/*    */ 
/*    */   public void resize(int width, int height)
/*    */   {
/* 71 */     this.viewPort.update(width, height);
/*    */   }
/*    */ 
/*    */   public void pause()
/*    */   {
/*    */   }
/*    */ 
/*    */   public void resume()
/*    */   {
/*    */   }
/*    */ 
/*    */   public void hide()
/*    */   {
/*    */   }
/*    */ 
/*    */   public void dispose()
/*    */   {
/* 91 */     Gdx.app.log("Splash", "Disposing");
/* 92 */     this.splash.dispose();
/* 93 */     this.batch.dispose();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.SplashScreen
 * JD-Core Version:    0.6.2
 */