/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level7 extends GameScreen
/*    */ {
/*    */   public Level7(Game game)
/*    */   {
/* 18 */     super(game, "lvl7.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 23 */     this.startPosition = new Vector2(2.0F, 2.5F);
/* 24 */     this.level = 7;
/* 25 */     super.show();
/* 26 */     this.player1.setColor(Color.GOLDENROD);
/*    */ 
/* 28 */     addOrb(Color.SKY, 29.5F, 5.4F);
/* 29 */     addOrb(Color.CHARTREUSE, 49.5F, 6.4F);
/* 30 */     addOrb(Color.GOLDENROD, 59.5F, 9.4F);
/* 31 */     addOrb(Color.MAROON, 88.5F, 2.4F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 39 */     this.prefs.putBoolean("level8", true);
/* 40 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 45 */     super.update(delta);
/*    */ 
/* 47 */     if (!this.player1.godMode)
/* 48 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.004F);
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 53 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 54 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 55 */     this.batch.begin();
/*    */ 
/* 57 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level7
 * JD-Core Version:    0.6.2
 */