/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level1 extends GameScreen
/*    */ {
/*    */   public Level1(Game game)
/*    */   {
/* 21 */     super(game, "lvl1.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 26 */     this.startPosition = new Vector2(1.5625F, 1.5625F);
/* 27 */     this.level = 1;
/* 28 */     super.show();
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 33 */     this.prefs.putBoolean("level2", true);
/* 34 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 39 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 40 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.05F, 0.05F, 1.0F));
/* 41 */     this.batch.begin();
/* 42 */     this.font.draw(this.batch, "Use 'A' and 'D' to move", 30.0F, 100.0F);
/* 43 */     this.font.draw(this.batch, "Press 'W' to jump", 300.0F, 100.0F);
/* 44 */     this.font.draw(this.batch, "The spikes will kill you!", 500.0F, 100.0F);
/* 45 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level1
 * JD-Core Version:    0.6.2
 */