/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level9 extends GameScreen
/*    */ {
/*    */   public Level9(Game game)
/*    */   {
/* 17 */     super(game, "lvl9.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 22 */     this.startPosition = new Vector2(2.0F, 2.5F);
/* 23 */     this.level = 9;
/* 24 */     super.show();
/*    */ 
/* 26 */     this.player1.setColor(Color.GOLDENROD);
/*    */ 
/* 28 */     addOrb(Color.SKY, 19.5F, 8.4F);
/* 29 */     addOrb(Color.SALMON, 35.5F, 9.4F);
/* 30 */     addOrb(Color.GOLDENROD, 49.5F, 2.4F);
/* 31 */     addOrb(Color.MAROON, 87.5F, 3.4F);
/* 32 */     addOrb(Color.TEAL, 101.5F, 7.4F);
/*    */ 
/* 34 */     addMplat(32.5F, 5.5F, 32.5F, 9.0F, Color.VIOLET, 0.2F);
/*    */ 
/* 36 */     addMplat(48.5F, 7.5F, 36.5F, 7.5F, Color.CYAN, 0.2F);
/*    */ 
/* 38 */     addMplat(35.5F, 2.0F, 39.5F, 6.0F, Color.ROYAL, 0.25F);
/*    */ 
/* 40 */     addMplat(87.5F, 1.5F, 95.550003F, 1.5F, Color.CYAN, 0.35F);
/* 41 */     addMplat(90.5F, 3.5F, 95.5F, 7.5F, Color.VIOLET, 0.3F);
/* 42 */     addMplat(95.5F, 3.5F, 90.5F, 7.5F, Color.GREEN, 0.25F);
/* 43 */     addMplat(90.5F, 8.5F, 93.5F, 8.5F, Color.CYAN, 0.35F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 49 */     this.prefs.putBoolean("level10", true);
/* 50 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 55 */     super.update(delta);
/*    */ 
/* 57 */     if (!this.player1.godMode)
/* 58 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.004F);
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 63 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 64 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 65 */     this.batch.begin();
/* 66 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level9
 * JD-Core Version:    0.6.2
 */