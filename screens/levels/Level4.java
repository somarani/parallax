/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level4 extends GameScreen
/*    */ {
/*    */   public Level4(Game game)
/*    */   {
/* 18 */     super(game, "lvl4.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 23 */     this.startPosition = new Vector2(1.0F, 7.5F);
/* 24 */     this.level = 4;
/* 25 */     super.show();
/* 26 */     this.player1.setColor(Color.ORANGE);
/*    */ 
/* 29 */     addOrb(Color.GREEN, 29.5F, 5.0F);
/* 30 */     addOrb(Color.NAVY, 50.5F, 1.3F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 35 */     this.prefs.putBoolean("level5", true);
/* 36 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 41 */     super.update(delta);
/*    */ 
/* 43 */     if (!this.player1.godMode)
/* 44 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.005F);
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level4
 * JD-Core Version:    0.6.2
 */