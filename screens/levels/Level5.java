/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level5 extends GameScreen
/*    */ {
/*    */   public Level5(Game game)
/*    */   {
/* 16 */     super(game, "lvl5.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 21 */     this.startPosition = new Vector2(2.0F, 2.5F);
/* 22 */     this.level = 5;
/* 23 */     super.show();
/* 24 */     this.player1.setColor(Color.MAROON);
/*    */ 
/* 27 */     addOrb(Color.CYAN, 45.5F, 8.0F);
/* 28 */     addOrb(Color.NAVY, 13.5F, 2.0F);
/* 29 */     addOrb(Color.GOLDENROD, 72.5F, 2.0F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 35 */     this.prefs.putBoolean("level6", true);
/* 36 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 41 */     super.update(delta);
/*    */ 
/* 43 */     if (!this.player1.godMode)
/* 44 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.005F);
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level5
 * JD-Core Version:    0.6.2
 */