/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level6 extends GameScreen
/*    */ {
/*    */   public Level6(Game game)
/*    */   {
/* 17 */     super(game, "lvl6.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 22 */     this.startPosition = new Vector2(2.0F, 2.5F);
/*    */ 
/* 24 */     this.level = 6;
/* 25 */     super.show();
/* 26 */     this.player1.setColor(Color.GOLDENROD);
/*    */ 
/* 28 */     addOrb(Color.SKY, 33.5F, 2.0F);
/* 29 */     addOrb(Color.CHARTREUSE, 56.5F, 2.0F);
/* 30 */     addOrb(Color.GOLDENROD, 72.5F, 8.0F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 37 */     this.prefs.putBoolean("level7", true);
/* 38 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 43 */     super.update(delta);
/*    */ 
/* 45 */     if (!this.player1.godMode)
/* 46 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.005F);
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 51 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 52 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 53 */     this.batch.begin();
/* 54 */     this.font.draw(this.batch, "Use the trampoline", 60.0F, 150.0F);
/* 55 */     this.font.draw(this.batch, "for a boost!", 60.0F, 135.0F);
/* 56 */     this.font.draw(this.batch, "Dont get stuck!", 2300.0F, 220.0F);
/*    */ 
/* 59 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level6
 * JD-Core Version:    0.6.2
 */