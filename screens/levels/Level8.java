/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level8 extends GameScreen
/*    */ {
/*    */   public Level8(Game game)
/*    */   {
/* 17 */     super(game, "lvl8.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 22 */     this.startPosition = new Vector2(2.0F, 2.5F);
/* 23 */     this.level = 8;
/* 24 */     super.show();
/*    */ 
/* 26 */     this.player1.setColor(Color.GOLDENROD);
/*    */ 
/* 28 */     addOrb(Color.SKY, 22.5F, 2.4F);
/* 29 */     addOrb(Color.SALMON, 35.5F, 9.4F);
/* 30 */     addOrb(Color.GOLDENROD, 66.5F, 7.4F);
/* 31 */     addOrb(Color.MAROON, 88.5F, 2.4F);
/* 32 */     addOrb(Color.TEAL, 103.5F, 2.4F);
/*    */ 
/* 34 */     addMplat(8.0F, 5.5F, 1.5F, 1.5F, Color.VIOLET, 0.2F);
/* 35 */     addMplat(10.5F, 6.5F, 18.5F, 6.5F, Color.CYAN, 0.2F);
/* 36 */     addMplat(32.5F, 1.5F, 32.5F, 9.0F, Color.CHARTREUSE, 0.2F);
/*    */ 
/* 38 */     addMplat(40.5F, 7.5F, 35.5F, 7.5F, Color.CYAN, 0.2F);
/* 39 */     addMplat(41.5F, 7.5F, 46.5F, 7.5F, Color.FIREBRICK, 0.2F);
/*    */ 
/* 41 */     addMplat(66.5F, 3.0F, 75.5F, 6.5F, Color.ROYAL, 0.2F);
/*    */ 
/* 43 */     addMplat(88.5F, 2.0F, 91.5F, 4.0F, Color.ROYAL, 0.35F);
/* 44 */     addMplat(91.5F, 6.0F, 88.5F, 6.0F, Color.VIOLET, 0.35F);
/* 45 */     addMplat(88.5F, 8.0F, 91.5F, 8.0F, Color.CYAN, 0.35F);
/*    */ 
/* 47 */     addMplat(117.5F, 5.5F, 105.5F, 5.5F, Color.CYAN, 0.25F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 54 */     this.prefs.putBoolean("level9", true);
/* 55 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 60 */     super.update(delta);
/*    */ 
/* 62 */     if (!this.player1.godMode)
/* 63 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.004F);
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 68 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 69 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 70 */     this.batch.begin();
/* 71 */     this.font.draw(this.batch, "Use the moving platform", 60.0F, 150.0F);
/* 72 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level8
 * JD-Core Version:    0.6.2
 */