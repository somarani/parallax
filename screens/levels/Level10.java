/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Door;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level10 extends GameScreen
/*    */ {
/*    */   Door door1;
/*    */   Door door2;
/*    */   Door door3;
/*    */   Door door4;
/*    */   Door door5;
/*    */ 
/*    */   public Level10(Game game)
/*    */   {
/* 24 */     super(game, "lvl10.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 29 */     this.startPosition = new Vector2(2.0F, 2.5F);
/*    */ 
/* 31 */     this.level = 10;
/* 32 */     super.show();
/*    */ 
/* 34 */     this.player1.setColor(Color.GOLDENROD);
/*    */ 
/* 36 */     addOrb(Color.SKY, 18.5F, 8.4F);
/* 37 */     addOrb(Color.SKY, 60.5F, 10.4F);
/* 38 */     addOrb(Color.SKY, 88.5F, 2.4F);
/*    */ 
/* 40 */     addMplat(15.5F, 1.5F, 9.5F, 1.5F, Color.VIOLET, 0.35F);
/* 41 */     addMplat(15.5F, 3.5F, 8.5F, 5.5F, Color.FIREBRICK, 0.3F);
/* 42 */     addMplat(9.5F, 6.5F, 16.5F, 6.5F, Color.NAVY, 0.3F);
/*    */ 
/* 44 */     addMplat(35.5F, 1.5F, 35.5F, 5.5F, Color.WHITE, 0.3F);
/*    */ 
/* 46 */     addMplat(78.5F, 7.5F, 85.5F, 7.5F, Color.YELLOW, 0.4F);
/* 47 */     addMplat(85.5F, 7.5F, 78.5F, 7.5F, Color.PURPLE, 0.4F);
/* 48 */     addMplat(78.5F, 4.5F, 85.5F, 4.5F, Color.ORANGE, 0.45F);
/* 49 */     addMplat(85.5F, 4.5F, 78.5F, 4.5F, Color.BLUE, 0.45F);
/* 50 */     addMplat(78.5F, 2.5F, 85.5F, 2.5F, Color.GREEN, 0.55F);
/* 51 */     addMplat(85.5F, 1.5F, 78.5F, 1.5F, Color.RED, 0.55F);
/*    */ 
/* 53 */     this.door1 = new Door(this, 7.5F, 1.5F, 7.5F, 4.4F, Color.CHARTREUSE);
/* 54 */     this.door2 = new Door(this, 17.5F, 7.5F, 14.5F, 10.4F, Color.BLUE);
/* 55 */     this.door3 = new Door(this, 77.5F, 8.5F, 72.5F, 8.4F, Color.RED);
/* 56 */     this.door4 = new Door(this, 99.5F, 8.5F, 99.5F, 3.4F, Color.GREEN);
/* 57 */     this.door5 = new Door(this, 114.5F, 8.5F, 120.5F, 2.4F, Color.YELLOW);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 63 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 68 */     super.update(delta);
/*    */ 
/* 70 */     if (!this.player1.godMode)
/* 71 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.003F);
/*    */   }
/*    */ 
/*    */   public void drawTextBehind()
/*    */   {
/* 76 */     if (!this.door1.isDone)
/* 77 */       this.door1.render();
/* 78 */     if (!this.door2.isDone)
/* 79 */       this.door2.render();
/* 80 */     if (!this.door3.isDone)
/* 81 */       this.door3.render();
/* 82 */     if (!this.door4.isDone)
/* 83 */       this.door4.render();
/* 84 */     if (!this.door5.isDone)
/* 85 */       this.door5.render();
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 90 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 91 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 92 */     this.batch.begin();
/* 93 */     if (!this.door1.isDone)
/* 94 */       this.font.draw(this.batch, "Collect the key to open the door", 60.0F, 150.0F);
/* 95 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level10
 * JD-Core Version:    0.6.2
 */