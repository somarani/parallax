/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.math.Matrix4;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level3 extends GameScreen
/*    */ {
/*    */   public Level3(Game game)
/*    */   {
/* 18 */     super(game, "lvl3.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 23 */     this.startPosition = new Vector2(2.0F, 2.5F);
/* 24 */     this.level = 3;
/* 25 */     super.show();
/* 26 */     this.player1.setColor(Color.VIOLET);
/*    */ 
/* 28 */     addOrb(Color.GOLDENROD, 29.5F, 4.0F);
/* 29 */     addOrb(Color.PURPLE, 50.5F, 2.0F);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 34 */     this.prefs.putBoolean("level4", true);
/* 35 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ 
/*    */   public void update(float delta)
/*    */   {
/* 40 */     super.update(delta);
/*    */ 
/* 42 */     if (!this.player1.godMode)
/* 43 */       this.player1.playerLight.setDistance(this.player1.playerLight.getDistance() - 0.005F);
/*    */   }
/*    */ 
/*    */   public void drawTextFront()
/*    */   {
/* 48 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 49 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.035F, 0.035F, 1.0F));
/* 50 */     this.batch.begin();
/* 51 */     this.font.draw(this.batch, "Oh no! Your light", 60.0F, 150.0F);
/* 52 */     this.font.draw(this.batch, "is beggining to die!", 60.0F, 135.0F);
/*    */ 
/* 54 */     this.font.draw(this.batch, "Find a light source quick!", 330.0F, 150.0F);
/*    */ 
/* 56 */     this.font.draw(this.batch, "Use the flowers light energy", 740.0F, 190.0F);
/*    */ 
/* 58 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level3
 * JD-Core Version:    0.6.2
 */