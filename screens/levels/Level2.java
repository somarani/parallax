/*    */ package com.compani.shooter.screens.levels;
/*    */ 
/*    */ import com.badlogic.gdx.Game;
/*    */ import com.badlogic.gdx.Preferences;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.compani.shooter.entities.Player;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ import com.compani.shooter.screens.LevelSelectScreen;
/*    */ 
/*    */ public class Level2 extends GameScreen
/*    */ {
/*    */   public Level2(Game game)
/*    */   {
/* 18 */     super(game, "lvl2.tmx");
/*    */   }
/*    */ 
/*    */   public void show()
/*    */   {
/* 23 */     this.startPosition = new Vector2(1.0F, 2.0F);
/* 24 */     this.level = 2;
/* 25 */     super.show();
/* 26 */     this.player1.setColor(Color.SKY);
/*    */   }
/*    */ 
/*    */   public void levelEnd()
/*    */   {
/* 31 */     this.prefs.putBoolean("level3", true);
/* 32 */     this.game.setScreen(new LevelSelectScreen(this.game));
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.levels.Level2
 * JD-Core Version:    0.6.2
 */