/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.Graphics.DisplayMode;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.Screen;
/*     */ import com.badlogic.gdx.graphics.GL20;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.badlogic.gdx.scenes.scene2d.InputEvent;
/*     */ import com.badlogic.gdx.scenes.scene2d.Stage;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.Skin;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
/*     */ import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
/*     */ import com.badlogic.gdx.utils.viewport.FillViewport;
/*     */ import com.badlogic.gdx.utils.viewport.Viewport;
/*     */ 
/*     */ public class SettingsScreen
/*     */   implements Screen
/*     */ {
/*     */   Game game;
/*     */   Stage stage;
/*     */   TextButton fullButton;
/*     */   TextButton res720Button;
/*     */   TextButton res1080Button;
/*     */   TextButton returnButton;
/*     */   TextButton key1;
/*     */   TextButton key2;
/*     */   TextButton.TextButtonStyle textButtonStyle;
/*     */   TextButton.TextButtonStyle textButtonStyle1;
/*     */   BitmapFont font;
/*     */   Skin skin;
/*     */   TextureAtlas buttonAtlas;
/*     */   SpriteBatch batch;
/*     */   Texture background;
/*     */   OrthographicCamera cam;
/*     */   Viewport viewPort;
/*     */ 
/*     */   public SettingsScreen(Game g)
/*     */   {
/*  65 */     this.game = g;
/*     */   }
/*     */ 
/*     */   public void show()
/*     */   {
/*  71 */     Gdx.app.log("Main", "Main Menu loaded");
/*     */ 
/*  74 */     this.cam = new OrthographicCamera();
/*  75 */     this.viewPort = new FillViewport(1920.0F, 1080.0F, this.cam);
/*  76 */     this.cam.position.set(this.viewPort.getWorldWidth() / 2.0F, this.viewPort.getWorldHeight() / 2.0F, 0.0F);
/*  77 */     this.cam.update();
/*     */ 
/*  80 */     this.stage = new Stage();
/*  81 */     this.font = new BitmapFont(Gdx.files.local("font.fnt"));
/*  82 */     this.skin = new Skin();
/*  83 */     this.buttonAtlas = new TextureAtlas("buttons.pack");
/*  84 */     this.skin.addRegions(this.buttonAtlas);
/*  85 */     this.batch = new SpriteBatch();
/*  86 */     this.background = new Texture("mainbg.png");
/*     */ 
/*  88 */     Gdx.input.setInputProcessor(this.stage);
/*     */ 
/*  91 */     this.textButtonStyle = new TextButton.TextButtonStyle();
/*  92 */     this.textButtonStyle.font = this.font;
/*  93 */     this.textButtonStyle.up = this.skin.getDrawable("unclicked");
/*  94 */     this.textButtonStyle.over = this.skin.getDrawable("hovered");
/*  95 */     this.textButtonStyle.down = this.skin.getDrawable("clicked");
/*  96 */     this.textButtonStyle.font.setColor(93.0F, 93.0F, 93.0F, 1.0F);
/*     */ 
/* 100 */     this.fullButton = new TextButton("Fullscreen", this.textButtonStyle);
/* 101 */     this.res720Button = new TextButton("720p", this.textButtonStyle);
/* 102 */     this.res1080Button = new TextButton("1080p", this.textButtonStyle);
/* 103 */     this.returnButton = new TextButton("Return", this.textButtonStyle);
/*     */ 
/* 105 */     if (com.compani.shooter.main.Constants.player1keys[0] == 32)
/* 106 */       this.key1 = new TextButton("Use WASD", this.textButtonStyle);
/*     */     else {
/* 108 */       this.key1 = new TextButton("Use Arrows", this.textButtonStyle);
/*     */     }
/*     */ 
/* 113 */     this.fullButton.setX(510.0F);
/* 114 */     this.fullButton.setY(425.0F);
/*     */ 
/* 116 */     this.res720Button.setX(810.0F);
/* 117 */     this.res720Button.setY(425.0F);
/*     */ 
/* 119 */     this.res1080Button.setX(1110.0F);
/* 120 */     this.res1080Button.setY(425.0F);
/*     */ 
/* 122 */     this.returnButton.setX(1110.0F);
/* 123 */     this.returnButton.setY(110.0F);
/*     */ 
/* 125 */     this.key1.setX(510.0F);
/* 126 */     this.key1.setY(270.0F);
/*     */ 
/* 129 */     this.res720Button.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 133 */         Gdx.graphics.setWindowedMode(1280, 720);
/* 134 */         com.compani.shooter.main.Constants.WIDTH = 1280;
/* 135 */         com.compani.shooter.main.Constants.HEIGHT = 720;
/*     */       }
/*     */     });
/* 141 */     this.res1080Button.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 145 */         Gdx.graphics.setWindowedMode(1920, 1080);
/* 146 */         com.compani.shooter.main.Constants.WIDTH = 1920;
/* 147 */         com.compani.shooter.main.Constants.HEIGHT = 1080;
/*     */       }
/*     */     });
/* 152 */     this.fullButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 156 */         Graphics.DisplayMode currentMode = Gdx.graphics.getDisplayMode();
/* 157 */         Gdx.graphics.setFullscreenMode(currentMode);
/*     */       }
/*     */     });
/* 163 */     this.returnButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/* 166 */         SettingsScreen.this.game.setScreen(new MainMenuScreen(SettingsScreen.this.game));
/*     */       }
/*     */     });
/* 171 */     this.key1.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 176 */         if (com.compani.shooter.main.Constants.player1keys[0] == 32) {
/* 177 */           com.compani.shooter.main.Constants.player1keys[0] = 22;
/* 178 */           com.compani.shooter.main.Constants.player1keys[1] = 21;
/* 179 */           com.compani.shooter.main.Constants.player1keys[2] = 19;
/* 180 */           com.compani.shooter.main.Constants.player1keys[3] = 20;
/*     */ 
/* 182 */           SettingsScreen.this.key1.setText("Use Arrows");
/*     */         }
/*     */         else
/*     */         {
/* 186 */           com.compani.shooter.main.Constants.player1keys[0] = 32;
/* 187 */           com.compani.shooter.main.Constants.player1keys[1] = 29;
/* 188 */           com.compani.shooter.main.Constants.player1keys[2] = 51;
/* 189 */           com.compani.shooter.main.Constants.player1keys[3] = 47;
/*     */ 
/* 191 */           SettingsScreen.this.key1.setText("Use WASD");
/*     */         }
/*     */       }
/*     */     });
/* 199 */     this.stage.addActor(this.fullButton);
/* 200 */     this.stage.addActor(this.res720Button);
/* 201 */     this.stage.addActor(this.res1080Button);
/* 202 */     this.stage.addActor(this.returnButton);
/* 203 */     this.stage.addActor(this.key1);
/* 204 */     this.stage.setViewport(this.viewPort);
/*     */   }
/*     */ 
/*     */   public void render(float delta)
/*     */   {
/* 211 */     Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 212 */     Gdx.gl.glClear(16384);
/*     */ 
/* 214 */     this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 216 */     this.batch.begin();
/* 217 */     this.batch.draw(this.background, 0.0F, 0.0F, 1920.0F, 1080.0F);
/* 218 */     this.batch.end();
/*     */ 
/* 220 */     this.stage.draw();
/* 221 */     this.stage.act();
/*     */   }
/*     */ 
/*     */   public void resize(int width, int height)
/*     */   {
/* 226 */     this.viewPort.update(width, height);
/*     */   }
/*     */ 
/*     */   public void pause()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resume()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void hide()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 246 */     this.stage.dispose();
/* 247 */     this.background.dispose();
/* 248 */     this.batch.dispose();
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.SettingsScreen
 * JD-Core Version:    0.6.2
 */