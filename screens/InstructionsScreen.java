/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.Screen;
/*     */ import com.badlogic.gdx.graphics.GL20;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.badlogic.gdx.scenes.scene2d.InputEvent;
/*     */ import com.badlogic.gdx.scenes.scene2d.Stage;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.Skin;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
/*     */ import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
/*     */ import com.badlogic.gdx.utils.viewport.StretchViewport;
/*     */ import com.badlogic.gdx.utils.viewport.Viewport;
/*     */ 
/*     */ public class InstructionsScreen
/*     */   implements Screen
/*     */ {
/*     */   Game game;
/*     */   Stage stage;
/*     */   TextButton exitButton;
/*     */   TextButton.TextButtonStyle textButtonStyle;
/*     */   BitmapFont font;
/*     */   Skin skin;
/*     */   TextureAtlas buttonAtlas;
/*     */   SpriteBatch batch;
/*     */   Texture background;
/*     */   OrthographicCamera cam;
/*     */   Viewport viewPort;
/*     */ 
/*     */   public InstructionsScreen(Game g)
/*     */   {
/*  47 */     this.game = g;
/*     */   }
/*     */ 
/*     */   public void show()
/*     */   {
/*  53 */     Gdx.app.log("Main", "Instructions Menu loaded");
/*     */ 
/*  56 */     this.cam = new OrthographicCamera();
/*  57 */     this.viewPort = new StretchViewport(1920.0F, 1080.0F, this.cam);
/*  58 */     this.cam.position.set(this.viewPort.getWorldWidth() / 2.0F, this.viewPort.getWorldHeight() / 2.0F, 0.0F);
/*  59 */     this.cam.update();
/*     */ 
/*  62 */     this.stage = new Stage();
/*  63 */     this.font = new BitmapFont(Gdx.files.local("font.fnt"));
/*  64 */     this.skin = new Skin();
/*  65 */     this.buttonAtlas = new TextureAtlas("buttons.pack");
/*  66 */     this.skin.addRegions(this.buttonAtlas);
/*  67 */     this.batch = new SpriteBatch();
/*  68 */     this.background = new Texture("instructions.jpg");
/*     */ 
/*  70 */     Gdx.input.setInputProcessor(this.stage);
/*     */ 
/*  73 */     this.textButtonStyle = new TextButton.TextButtonStyle();
/*  74 */     this.textButtonStyle.font = this.font;
/*  75 */     this.textButtonStyle.up = this.skin.getDrawable("unclicked");
/*  76 */     this.textButtonStyle.over = this.skin.getDrawable("hovered");
/*  77 */     this.textButtonStyle.down = this.skin.getDrawable("clicked");
/*  78 */     this.textButtonStyle.font.setColor(93.0F, 93.0F, 93.0F, 1.0F);
/*     */ 
/*  80 */     this.exitButton = new TextButton("Back", this.textButtonStyle);
/*     */ 
/*  83 */     this.exitButton.setX(234.0F);
/*  84 */     this.exitButton.setY(106.0F);
/*     */ 
/*  86 */     this.exitButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/*  89 */         Gdx.app.log("Main", "Return Button Pressed");
/*  90 */         InstructionsScreen.this.game.setScreen(new MainMenuScreen(InstructionsScreen.this.game));
/*     */       }
/*     */     });
/*  96 */     this.stage.addActor(this.exitButton);
/*  97 */     this.stage.setViewport(this.viewPort);
/*     */   }
/*     */ 
/*     */   public void render(float delta)
/*     */   {
/* 103 */     Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 104 */     Gdx.gl.glClear(16384);
/*     */ 
/* 106 */     this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 108 */     this.batch.begin();
/* 109 */     this.batch.draw(this.background, 0.0F, 0.0F, 1920.0F, 1080.0F);
/* 110 */     this.batch.end();
/*     */ 
/* 112 */     this.stage.draw();
/* 113 */     this.stage.act();
/*     */   }
/*     */ 
/*     */   public void resize(int width, int height)
/*     */   {
/* 118 */     this.viewPort.update(width, height);
/*     */   }
/*     */ 
/*     */   public void pause()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resume()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void hide()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 138 */     this.stage.dispose();
/* 139 */     this.background.dispose();
/* 140 */     this.batch.dispose();
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.InstructionsScreen
 * JD-Core Version:    0.6.2
 */