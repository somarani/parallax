/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import box2dLight.PointLight;
/*     */ import box2dLight.RayHandler;
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Preferences;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.GL20;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.maps.MapLayer;
/*     */ import com.badlogic.gdx.maps.MapLayers;
/*     */ import com.badlogic.gdx.maps.MapObject;
/*     */ import com.badlogic.gdx.maps.MapObjects;
/*     */ import com.badlogic.gdx.maps.objects.RectangleMapObject;
/*     */ import com.badlogic.gdx.maps.tiled.TiledMap;
/*     */ import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
/*     */ import com.badlogic.gdx.math.Matrix4;
/*     */ import com.badlogic.gdx.math.Rectangle;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*     */ import com.badlogic.gdx.physics.box2d.Contact;
/*     */ import com.badlogic.gdx.physics.box2d.ContactImpulse;
/*     */ import com.badlogic.gdx.physics.box2d.ContactListener;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.FixtureDef;
/*     */ import com.badlogic.gdx.physics.box2d.Manifold;
/*     */ import com.badlogic.gdx.physics.box2d.PolygonShape;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.badlogic.gdx.utils.viewport.ScreenViewport;
/*     */ import com.badlogic.gdx.utils.viewport.Viewport;
/*     */ import com.compani.shooter.entities.Player;
/*     */ import com.compani.shooter.screens.levels.Level1;
/*     */ import com.compani.shooter.screens.levels.Level10;
/*     */ import com.compani.shooter.screens.levels.Level2;
/*     */ import com.compani.shooter.screens.levels.Level3;
/*     */ import com.compani.shooter.screens.levels.Level4;
/*     */ import com.compani.shooter.screens.levels.Level5;
/*     */ import com.compani.shooter.screens.levels.Level6;
/*     */ import com.compani.shooter.screens.levels.Level7;
/*     */ import com.compani.shooter.screens.levels.Level8;
/*     */ import com.compani.shooter.screens.levels.Level9;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ public class LevelSelectScreen extends GameScreen
/*     */ {
/*  32 */   boolean[] lvl = new boolean[12];
/*  33 */   float[] beginTime = new float[12];
/*     */ 
/*  35 */   GameScreen[] lvls = new GameScreen[12];
/*     */ 
/*  37 */   public Body[] body = new Body[12];
/*     */ 
/*  40 */   PointLight[] lights = new PointLight[12];
/*     */ 
/*     */   public LevelSelectScreen(Game game) {
/*  43 */     super(game, "levelselect.tmx");
/*     */   }
/*     */ 
/*     */   public void show()
/*     */   {
/*  49 */     this.prefs = Gdx.app.getPreferences("level");
/*  50 */     this.prefs.putBoolean("level1", true);
/*     */ 
/*  66 */     if (this.prefs.getBoolean("level1"))
/*  67 */       this.lvl[1] = true;
/*  68 */     if (this.prefs.getBoolean("level2"))
/*  69 */       this.lvl[2] = true;
/*  70 */     if (this.prefs.getBoolean("level3"))
/*  71 */       this.lvl[3] = true;
/*  72 */     if (this.prefs.getBoolean("level4"))
/*  73 */       this.lvl[4] = true;
/*  74 */     if (this.prefs.getBoolean("level5"))
/*  75 */       this.lvl[5] = true;
/*  76 */     if (this.prefs.getBoolean("level6"))
/*  77 */       this.lvl[6] = true;
/*  78 */     if (this.prefs.getBoolean("level7"))
/*  79 */       this.lvl[7] = true;
/*  80 */     if (this.prefs.getBoolean("level8"))
/*  81 */       this.lvl[8] = true;
/*  82 */     if (this.prefs.getBoolean("level9"))
/*  83 */       this.lvl[9] = true;
/*  84 */     if (this.prefs.getBoolean("level10")) {
/*  85 */       this.lvl[10] = true;
/*     */     }
/*  87 */     this.lvl[11] = true;
/*     */ 
/*  89 */     this.lvls[1] = new Level1(this.game);
/*  90 */     this.lvls[2] = new Level2(this.game);
/*  91 */     this.lvls[3] = new Level3(this.game);
/*  92 */     this.lvls[4] = new Level4(this.game);
/*  93 */     this.lvls[5] = new Level5(this.game);
/*  94 */     this.lvls[6] = new Level6(this.game);
/*  95 */     this.lvls[7] = new Level7(this.game);
/*  96 */     this.lvls[8] = new Level8(this.game);
/*  97 */     this.lvls[9] = new Level9(this.game);
/*  98 */     this.lvls[10] = new Level10(this.game);
/*     */ 
/* 100 */     this.startPosition = new Vector2(1.5625F, 1.5625F);
/*     */ 
/* 102 */     super.show();
/*     */ 
/* 104 */     this.player1.lightLength = 1.0F;
/*     */ 
/* 108 */     initLight(1, Color.ROYAL);
/* 109 */     initLight(2, Color.PINK);
/* 110 */     initLight(3, Color.PURPLE);
/* 111 */     initLight(4, Color.LIME);
/* 112 */     initLight(5, Color.CHARTREUSE);
/* 113 */     initLight(6, Color.YELLOW);
/* 114 */     initLight(7, Color.SALMON);
/* 115 */     initLight(8, Color.RED);
/* 116 */     initLight(9, Color.SKY);
/* 117 */     initLight(10, Color.PURPLE);
/*     */ 
/* 119 */     this.player1.setColor(Color.SKY);
/* 120 */     this.player1.playerLight.setDistance(1.0F);
/*     */ 
/* 122 */     this.world.setContactListener(new ListenerClass());
/*     */ 
/* 124 */     this.player1.jumpCount = 1;
/*     */ 
/* 126 */     this.level = 0;
/*     */ 
/* 128 */     this.HUDbatch.setProjectionMatrix(this.HUDbatch.getProjectionMatrix().scale(1.5F, 1.5F, 1.0F));
/*     */   }
/*     */ 
/*     */   public void setCam()
/*     */   {
/* 133 */     this.cam = new OrthographicCamera();
/* 134 */     this.viewPort = new ScreenViewport(this.cam);
/* 135 */     this.cam.zoom = 0.015625F;
/* 136 */     this.cam.position.set(this.viewPort.getWorldWidth() / 2.0F + 20.0F, this.viewPort.getWorldHeight() / 2.0F + 2.0F, 0.0F);
/* 137 */     this.cam.update();
/*     */   }
/*     */ 
/*     */   public void initLight(int x, Color color) {
/* 141 */     if (this.lvl[x] != 0) {
/* 142 */       this.lights[x] = new PointLight(this.rayHandler, 200, color, 4.0F, 0.0F, 0.0F);
/* 143 */       this.lights[x].setXray(true);
/* 144 */       this.lights[x].setSoftnessLength(3.0F);
/* 145 */       this.lights[x].setPosition(2.5F + 3 * (x - 1), 3.5F);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void update(float delta)
/*     */   {
/* 152 */     this.world.step(0.01666667F, 6, 2);
/*     */ 
/* 154 */     float lerp = 0.8F;
/* 155 */     Vector3 position = this.cam.position;
/*     */     Vector3 tmp24_23 = position; tmp24_23.x = ((float)(tmp24_23.x + (this.player1.body.getPosition().x + 3.5D - position.x) * lerp * delta));
/*     */ 
/* 160 */     this.rayHandler.update();
/*     */ 
/* 163 */     this.cam.update();
/*     */ 
/* 165 */     if (this.player1.body.getLinearVelocity().y == 0.0F) {
/* 166 */       this.player1.canJump = true;
/*     */     }
/* 168 */     for (int i = 1; i <= 11; i++)
/* 169 */       animation(i);
/*     */   }
/*     */ 
/*     */   public void animation(int x)
/*     */   {
/* 175 */     if ((this.beginTime[x] > 0.0F) && (this.lvl[(x - 1)] != 0)) {
/* 176 */       this.player1.body.setActive(false);
/* 177 */       this.player1.playerLight.setDistance(0.0F);
/* 178 */       for (int i = 1; i < 11; i++) {
/* 179 */         if ((this.lvl[i] != 0) && (i != x - 1)) {
/* 180 */           this.lights[i].setDistance(this.lights[i].getDistance() - 0.1F);
/*     */         }
/* 182 */         this.lights[(x - 1)].setDistance(this.lights[(x - 1)].getDistance() + 0.03F);
/*     */       }
/* 184 */       if ((float)System.nanoTime() - this.beginTime[x] > 1.0E+009F)
/* 185 */         this.game.setScreen(this.lvls[(x - 1)]);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setMap()
/*     */   {
/* 192 */     BodyDef bodyDef = new BodyDef();
/* 193 */     PolygonShape shape = new PolygonShape();
/* 194 */     FixtureDef fdef = new FixtureDef();
/*     */ 
/* 199 */     for (MapObject object : this.map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
/* 200 */       Rectangle rect = ((RectangleMapObject)object).getRectangle();
/*     */ 
/* 202 */       bodyDef.type = BodyDef.BodyType.StaticBody;
/* 203 */       bodyDef.position.set(rect.getX() * 0.015625F + rect.getWidth() * 0.015625F / 2.0F, rect.getY() * 0.015625F + rect.getHeight() * 0.015625F / 2.0F);
/*     */ 
/* 205 */       Body bodyGround = this.world.createBody(bodyDef);
/* 206 */       shape.setAsBox(rect.getWidth() * 0.015625F / 2.0F, rect.getHeight() * 0.015625F / 2.0F);
/* 207 */       fdef.shape = shape;
/* 208 */       bodyGround.createFixture(fdef);
/*     */     }
/*     */ 
/* 212 */     for (int i = 1; i <= 11; i++)
/* 213 */       createBody(i);
/*     */   }
/*     */ 
/*     */   public void createBody(int i) {
/* 217 */     BodyDef bodyDef = new BodyDef();
/* 218 */     PolygonShape shape = new PolygonShape();
/* 219 */     FixtureDef fdef = new FixtureDef();
/*     */ 
/* 221 */     if (this.lvl[(i - 1)] != 0)
/*     */     {
/* 223 */       for (MapObject object : this.map.getLayers().get(i + 1).getObjects().getByType(RectangleMapObject.class)) {
/* 224 */         Rectangle rect = ((RectangleMapObject)object).getRectangle();
/*     */ 
/* 226 */         bodyDef.type = BodyDef.BodyType.StaticBody;
/* 227 */         bodyDef.position.set(rect.getX() * 0.015625F + rect.getWidth() * 0.015625F / 2.0F, rect.getY() * 0.015625F + rect.getHeight() * 0.015625F / 2.0F);
/*     */ 
/* 229 */         this.body[i] = this.world.createBody(bodyDef);
/* 230 */         shape.setAsBox(rect.getWidth() * 0.015625F / 2.0F, rect.getHeight() * 0.015625F / 2.0F);
/* 231 */         fdef.shape = shape;
/* 232 */         fdef.isSensor = true;
/* 233 */         this.body[i].createFixture(fdef);
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   public void resize(int width, int height)
/*     */   {
/* 315 */     this.viewPort.update(width, height);
/* 316 */     this.rayHandler.useCustomViewport(this.viewPort.getScreenX(), this.viewPort.getScreenY(), width, height);
/*     */   }
/*     */ 
/*     */   public void render(float delta)
/*     */   {
/* 322 */     Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 323 */     Gdx.gl.glClear(16384);
/*     */ 
/* 325 */     update(delta);
/*     */ 
/* 329 */     this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 331 */     this.renderer.setView(this.cam);
/* 332 */     this.renderer.render();
/*     */ 
/* 334 */     this.batch.begin();
/* 335 */     this.batch.end();
/*     */ 
/* 338 */     this.player1.render();
/*     */ 
/* 340 */     Matrix4 originalMatrix = this.batch.getProjectionMatrix().cpy();
/* 341 */     this.batch.setProjectionMatrix(originalMatrix.cpy().scale(0.1F, 0.1F, 1.0F));
/* 342 */     this.batch.begin();
/* 343 */     this.font.draw(this.batch, "1", 20.0F, 50.0F);
/* 344 */     this.font.draw(this.batch, "2", 50.0F, 50.0F);
/* 345 */     this.font.draw(this.batch, "3", 80.0F, 50.0F);
/* 346 */     this.font.draw(this.batch, "4", 110.0F, 50.0F);
/* 347 */     this.font.draw(this.batch, "5", 140.0F, 50.0F);
/* 348 */     this.font.draw(this.batch, "6", 170.0F, 50.0F);
/* 349 */     this.font.draw(this.batch, "7", 200.0F, 50.0F);
/* 350 */     this.font.draw(this.batch, "8", 230.0F, 50.0F);
/* 351 */     this.font.draw(this.batch, "9", 260.0F, 50.0F);
/* 352 */     this.font.draw(this.batch, "10", 285.0F, 50.0F);
/*     */ 
/* 354 */     this.batch.end();
/* 355 */     this.batch.setProjectionMatrix(originalMatrix);
/*     */ 
/* 357 */     this.rayHandler.setCombinedMatrix(this.cam.combined);
/* 358 */     this.rayHandler.render();
/*     */ 
/* 362 */     float x = Math.round(this.player1.body.getPosition().x * 100.0F) / 100.0F;
/* 363 */     float y = Math.round(this.player1.body.getPosition().y * 100.0F) / 100.0F;
/* 364 */     float velx = Math.round(this.player1.body.getLinearVelocity().x * 100.0F) / 100.0F;
/* 365 */     float vely = Math.round(this.player1.body.getLinearVelocity().y * 100.0F) / 100.0F;
/* 366 */     float light = Math.round(this.player1.playerLight.getDistance() / this.player1.lightLength * 100.0F * 100.0F) / 100.0F;
/*     */ 
/* 368 */     this.HUDbatch.begin();
/*     */ 
/* 375 */     this.HUDbatch.end();
/*     */   }
/*     */ 
/*     */   public class ListenerClass
/*     */     implements ContactListener
/*     */   {
/*     */     public ListenerClass()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void endContact(Contact contact)
/*     */     {
/* 242 */       Fixture fixtureA = contact.getFixtureA();
/* 243 */       Fixture fixtureB = contact.getFixtureB();
/*     */ 
/* 245 */       if ((fixtureB.getBody().getUserData() == "ground") && (fixtureA.getBody().getUserData() == "sensor")) {
/* 246 */         LevelSelectScreen.this.player1.jumpCount -= 1;
/* 247 */         System.out.println("subtracted: " + LevelSelectScreen.this.player1.jumpCount);
/*     */       }
/* 249 */       else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "mplat")) {
/* 250 */         LevelSelectScreen.this.player1.jumpCount -= 1;
/* 251 */       } else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "trampoline")) {
/* 252 */         LevelSelectScreen.this.player1.jumpCount -= 1;
/*     */       }
/* 254 */       if (LevelSelectScreen.this.player1.jumpCount < 0)
/* 255 */         LevelSelectScreen.this.player1.jumpCount = 0;
/*     */     }
/*     */ 
/*     */     public void beginContact(Contact contact)
/*     */     {
/* 262 */       if (LevelSelectScreen.this.player1.hasJumped)
/*     */       {
/* 264 */         LevelSelectScreen.this.player1.hasJumped = false;
/*     */       }
/*     */ 
/* 267 */       Fixture fixtureA = contact.getFixtureA();
/* 268 */       Fixture fixtureB = contact.getFixtureB();
/*     */ 
/* 270 */       if ((fixtureA.getBody().getUserData() == "ground") && (fixtureB.getBody().getUserData() == "sensor")) {
/* 271 */         LevelSelectScreen.this.player1.jumpCount += 1;
/*     */       }
/*     */ 
/* 274 */       if ((fixtureB.getBody().getUserData() == "ground") && (fixtureA.getBody().getUserData() == "sensor")) {
/* 275 */         LevelSelectScreen.this.player1.jumpCount += 1;
/*     */       }
/*     */ 
/* 278 */       if (fixtureB.getBody() == LevelSelectScreen.this.player1.body)
/* 279 */         if (fixtureA.getBody() == LevelSelectScreen.this.body[1])
/* 280 */           LevelSelectScreen.this.beginTime[1] = ((float)System.nanoTime());
/* 281 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[2])
/* 282 */           LevelSelectScreen.this.beginTime[2] = ((float)System.nanoTime());
/* 283 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[3])
/* 284 */           LevelSelectScreen.this.beginTime[3] = ((float)System.nanoTime());
/* 285 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[4])
/* 286 */           LevelSelectScreen.this.beginTime[4] = ((float)System.nanoTime());
/* 287 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[5])
/* 288 */           LevelSelectScreen.this.beginTime[5] = ((float)System.nanoTime());
/* 289 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[6])
/* 290 */           LevelSelectScreen.this.beginTime[6] = ((float)System.nanoTime());
/* 291 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[7])
/* 292 */           LevelSelectScreen.this.beginTime[7] = ((float)System.nanoTime());
/* 293 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[8])
/* 294 */           LevelSelectScreen.this.beginTime[8] = ((float)System.nanoTime());
/* 295 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[9])
/* 296 */           LevelSelectScreen.this.beginTime[9] = ((float)System.nanoTime());
/* 297 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[10])
/* 298 */           LevelSelectScreen.this.beginTime[10] = ((float)System.nanoTime());
/* 299 */         else if (fixtureA.getBody() == LevelSelectScreen.this.body[11])
/* 300 */           LevelSelectScreen.this.beginTime[11] = ((float)System.nanoTime());
/*     */     }
/*     */ 
/*     */     public void preSolve(Contact contact, Manifold oldManifold)
/*     */     {
/*     */     }
/*     */ 
/*     */     public void postSolve(Contact contact, ContactImpulse impulse)
/*     */     {
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.LevelSelectScreen
 * JD-Core Version:    0.6.2
 */