/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import com.badlogic.gdx.maps.Map;
/*     */ import com.badlogic.gdx.maps.MapLayer;
/*     */ import com.badlogic.gdx.maps.MapLayers;
/*     */ import com.badlogic.gdx.maps.MapObject;
/*     */ import com.badlogic.gdx.maps.MapObjects;
/*     */ import com.badlogic.gdx.maps.objects.CircleMapObject;
/*     */ import com.badlogic.gdx.maps.objects.PolygonMapObject;
/*     */ import com.badlogic.gdx.maps.objects.PolylineMapObject;
/*     */ import com.badlogic.gdx.maps.objects.RectangleMapObject;
/*     */ import com.badlogic.gdx.maps.objects.TextureMapObject;
/*     */ import com.badlogic.gdx.math.Circle;
/*     */ import com.badlogic.gdx.math.Polygon;
/*     */ import com.badlogic.gdx.math.Polyline;
/*     */ import com.badlogic.gdx.math.Rectangle;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*     */ import com.badlogic.gdx.physics.box2d.ChainShape;
/*     */ import com.badlogic.gdx.physics.box2d.CircleShape;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.PolygonShape;
/*     */ import com.badlogic.gdx.physics.box2d.Shape;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.badlogic.gdx.utils.Array;
/*     */ 
/*     */ public class MapBodyBuilder
/*     */ {
/*  19 */   private static float ppt = 0.0F;
/*     */ 
/*     */   public static Array<Body> buildShapes(Map map, int add, float pixels, World world) {
/*  22 */     ppt = pixels;
/*     */ 
/*  24 */     MapObjects objects = map.getLayers().get(add).getObjects();
/*     */ 
/*  26 */     Array bodies = new Array();
/*     */ 
/*  28 */     for (MapObject object : objects)
/*     */     {
/*  30 */       if (!(object instanceof TextureMapObject))
/*     */       {
/*     */         Shape shape;
/*     */         Shape shape;
/*  36 */         if ((object instanceof RectangleMapObject)) {
/*  37 */           shape = getRectangle((RectangleMapObject)object);
/*     */         }
/*     */         else
/*     */         {
/*     */           Shape shape;
/*  39 */           if ((object instanceof PolygonMapObject)) {
/*  40 */             shape = getPolygon((PolygonMapObject)object);
/*     */           }
/*     */           else
/*     */           {
/*     */             Shape shape;
/*  42 */             if ((object instanceof PolylineMapObject)) {
/*  43 */               shape = getPolyline((PolylineMapObject)object);
/*     */             } else {
/*  45 */               if (!(object instanceof CircleMapObject)) continue;
/*  46 */               shape = getCircle((CircleMapObject)object);
/*     */             }
/*     */           }
/*     */ 
/*     */         }
/*     */ 
/*  52 */         BodyDef bd = new BodyDef();
/*     */ 
/*  54 */         bd.type = BodyDef.BodyType.StaticBody;
/*  55 */         Body body = world.createBody(bd);
/*  56 */         body.createFixture(shape, 1.0F);
/*     */ 
/*  58 */         bodies.add(body);
/*     */ 
/*  60 */         if (add == 2) {
/*  61 */           body.setUserData("ground");
/*     */         }
/*     */ 
/*  64 */         if (add == 3) {
/*  65 */           body.setUserData("spike");
/*     */         }
/*  67 */         if (add == 4) {
/*  68 */           body.setUserData("end");
/*     */         }
/*  70 */         if (add == 5) {
/*  71 */           body.setUserData("trampoline");
/*  72 */           ((Fixture)body.getFixtureList().get(0)).setRestitution(1.175F);
/*     */         }
/*     */ 
/*  75 */         shape.dispose();
/*     */       }
/*     */     }
/*  77 */     return bodies;
/*     */   }
/*     */ 
/*     */   private static PolygonShape getRectangle(RectangleMapObject rectangleObject) {
/*  81 */     Rectangle rectangle = rectangleObject.getRectangle();
/*  82 */     PolygonShape polygon = new PolygonShape();
/*  83 */     Vector2 size = new Vector2((rectangle.x + rectangle.width * 0.5F) / ppt, (rectangle.y + rectangle.height * 0.5F) / ppt);
/*     */ 
/*  85 */     polygon.setAsBox(rectangle.width * 0.5F / ppt, rectangle.height * 0.5F / ppt, size, 0.0F);
/*     */ 
/*  89 */     return polygon;
/*     */   }
/*     */ 
/*     */   private static CircleShape getCircle(CircleMapObject circleObject) {
/*  93 */     Circle circle = circleObject.getCircle();
/*  94 */     CircleShape circleShape = new CircleShape();
/*  95 */     circleShape.setRadius(circle.radius / ppt);
/*  96 */     circleShape.setPosition(new Vector2(circle.x / ppt, circle.y / ppt));
/*  97 */     return circleShape;
/*     */   }
/*     */ 
/*     */   private static PolygonShape getPolygon(PolygonMapObject polygonObject) {
/* 101 */     PolygonShape polygon = new PolygonShape();
/* 102 */     float[] vertices = polygonObject.getPolygon().getTransformedVertices();
/*     */ 
/* 104 */     float[] worldVertices = new float[vertices.length];
/*     */ 
/* 106 */     for (int i = 0; i < vertices.length; i++) {
/* 107 */       vertices[i] /= ppt;
/*     */     }
/*     */ 
/* 110 */     polygon.set(worldVertices);
/* 111 */     return polygon;
/*     */   }
/*     */ 
/*     */   private static ChainShape getPolyline(PolylineMapObject polylineObject) {
/* 115 */     float[] vertices = polylineObject.getPolyline().getTransformedVertices();
/* 116 */     Vector2[] worldVertices = new Vector2[vertices.length / 2];
/*     */ 
/* 118 */     for (int i = 0; i < vertices.length / 2; i++) {
/* 119 */       worldVertices[i] = new Vector2();
/* 120 */       worldVertices[i].x = (vertices[(i * 2)] / ppt);
/* 121 */       worldVertices[i].y = (vertices[(i * 2 + 1)] / ppt);
/*     */     }
/*     */ 
/* 124 */     ChainShape chain = new ChainShape();
/* 125 */     chain.createChain(worldVertices);
/* 126 */     return chain;
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.MapBodyBuilder
 * JD-Core Version:    0.6.2
 */