/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import box2dLight.PointLight;
/*     */ import box2dLight.RayHandler;
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.InputMultiplexer;
/*     */ import com.badlogic.gdx.Preferences;
/*     */ import com.badlogic.gdx.Screen;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.GL20;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
/*     */ import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
/*     */ import com.badlogic.gdx.maps.tiled.TiledMap;
/*     */ import com.badlogic.gdx.maps.tiled.TmxMapLoader;
/*     */ import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
/*     */ import com.badlogic.gdx.math.Matrix4;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
/*     */ import com.badlogic.gdx.physics.box2d.Contact;
/*     */ import com.badlogic.gdx.physics.box2d.ContactImpulse;
/*     */ import com.badlogic.gdx.physics.box2d.ContactListener;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.Manifold;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.badlogic.gdx.scenes.scene2d.InputEvent;
/*     */ import com.badlogic.gdx.scenes.scene2d.Stage;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.Skin;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
/*     */ import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
/*     */ import com.badlogic.gdx.utils.Array;
/*     */ import com.badlogic.gdx.utils.viewport.ScreenViewport;
/*     */ import com.badlogic.gdx.utils.viewport.Viewport;
/*     */ import com.compani.shooter.entities.MovingPlatform;
/*     */ import com.compani.shooter.entities.Orb;
/*     */ import com.compani.shooter.entities.Player;
/*     */ import com.compani.shooter.main.Constants;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class GameScreen
/*     */   implements Screen
/*     */ {
/*     */   Stage stage;
/*     */   TextButton playButton;
/*     */   TextButton exitButton;
/*     */   TextButton settingButton;
/*     */   TextButton instructionButton;
/*     */   TextButton.TextButtonStyle textButtonStyle;
/*     */   Skin skin;
/*     */   BitmapFont font2;
/*     */   TextureAtlas buttonAtlas;
/*  53 */   ArrayList<Orb> orbs = new ArrayList();
/*  54 */   ArrayList<MovingPlatform> mplats = new ArrayList();
/*     */   public Preferences prefs;
/*  62 */   public State state = State.Running;
/*     */   public Game game;
/*     */   public int level;
/*     */   int collisionNum;
/*     */   public Player player1;
/*     */   public OrthographicCamera cam;
/*     */   Viewport viewPort;
/*  77 */   public boolean playerDead = false;
/*     */ 
/*  79 */   public boolean showDebug = false;
/*     */   public Vector2 startPosition;
/*     */   public SpriteBatch batch;
/*     */   public SpriteBatch HUDbatch;
/*     */   public BitmapFont font;
/*     */   FreeTypeFontGenerator generator;
/*     */   FreeTypeFontGenerator.FreeTypeFontParameter parameter;
/*     */   FreeTypeFontGenerator HUDgenerator;
/*     */   FreeTypeFontGenerator.FreeTypeFontParameter HUDparameter;
/*     */   InputMultiplexer inputMultiplexer;
/*  96 */   protected float ambientLight = 0.0F;
/*     */   TmxMapLoader mapLoader;
/*     */   TiledMap map;
/*     */   OrthogonalTiledMapRenderer renderer;
/*     */   public World world;
/*     */   Box2DDebugRenderer debugRenderer;
/*     */   String mapName;
/*     */   public float ppt;
/*     */   public RayHandler rayHandler;
/*     */ 
/*     */   public GameScreen(Game g, String mapName)
/*     */   {
/* 114 */     this.game = g;
/* 115 */     this.mapName = mapName;
/*     */   }
/*     */ 
/*     */   public void show()
/*     */   {
/* 121 */     this.prefs = Gdx.app.getPreferences("level");
/*     */ 
/* 124 */     this.batch = new SpriteBatch();
/* 125 */     this.HUDbatch = new SpriteBatch();
/*     */ 
/* 127 */     this.HUDbatch.setProjectionMatrix(this.HUDbatch.getProjectionMatrix().scale(2.0F, 2.0F, 1.0F));
/*     */ 
/* 130 */     this.world = new World(new Vector2(0.0F, -9.81F), true);
/* 131 */     this.debugRenderer = new Box2DDebugRenderer();
/*     */ 
/* 134 */     this.rayHandler = new RayHandler(this.world);
/* 135 */     this.rayHandler.setAmbientLight(this.ambientLight);
/*     */ 
/* 137 */     this.mapLoader = new TmxMapLoader();
/* 138 */     this.map = this.mapLoader.load(this.mapName);
/* 139 */     this.renderer = new OrthogonalTiledMapRenderer(this.map, 0.015625F);
/*     */ 
/* 142 */     this.player1 = new Player(this, Color.GOLDENROD, Constants.player1keys);
/*     */ 
/* 144 */     this.player1.body.setTransform(this.startPosition, 0.0F);
/*     */ 
/* 147 */     setCam();
/*     */ 
/* 152 */     this.generator = new FreeTypeFontGenerator(Gdx.files.local("emulogic.ttf"));
/* 153 */     this.parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
/* 154 */     this.parameter.size = 8;
/* 155 */     this.font = this.generator.generateFont(this.parameter);
/* 156 */     this.generator.dispose();
/*     */ 
/* 165 */     setMap();
/*     */ 
/* 167 */     this.world.setContactListener(new ListenerClass());
/*     */ 
/* 172 */     this.stage = new Stage();
/* 173 */     this.font2 = new BitmapFont(Gdx.files.local("font.fnt"));
/* 174 */     this.skin = new Skin();
/* 175 */     this.buttonAtlas = new TextureAtlas("buttons.pack");
/* 176 */     this.skin.addRegions(this.buttonAtlas);
/*     */ 
/* 178 */     Gdx.input.setInputProcessor(this.stage);
/*     */ 
/* 181 */     this.textButtonStyle = new TextButton.TextButtonStyle();
/* 182 */     this.textButtonStyle.font = this.font2;
/* 183 */     this.textButtonStyle.up = this.skin.getDrawable("unclicked");
/* 184 */     this.textButtonStyle.over = this.skin.getDrawable("hovered");
/* 185 */     this.textButtonStyle.down = this.skin.getDrawable("clicked");
/* 186 */     this.textButtonStyle.font.setColor(93.0F, 93.0F, 93.0F, 1.0F);
/*     */ 
/* 189 */     this.playButton = new TextButton("Resume", this.textButtonStyle);
/* 190 */     this.exitButton = new TextButton("Exit", this.textButtonStyle);
/* 191 */     this.instructionButton = new TextButton("Level Select", this.textButtonStyle);
/* 192 */     this.settingButton = new TextButton("Main Menu", this.textButtonStyle);
/*     */ 
/* 195 */     this.playButton.setX(-100.0F);
/* 196 */     this.playButton.setY(200.0F);
/*     */ 
/* 198 */     this.instructionButton.setX(-100.0F);
/* 199 */     this.instructionButton.setY(65.0F);
/*     */ 
/* 201 */     this.settingButton.setX(-100.0F);
/* 202 */     this.settingButton.setY(-70.0F);
/*     */ 
/* 204 */     this.exitButton.setX(-100.0F);
/* 205 */     this.exitButton.setY(-205.0F);
/*     */ 
/* 208 */     this.playButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 212 */         GameScreen.this.state = GameScreen.State.Running;
/*     */       }
/*     */     });
/* 218 */     this.instructionButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 222 */         GameScreen.this.game.setScreen(new LevelSelectScreen(GameScreen.this.game));
/*     */       }
/*     */     });
/* 227 */     this.exitButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/* 230 */         Gdx.app.log("Main", "Exit Button Pressed");
/* 231 */         Gdx.app.exit();
/*     */       }
/*     */     });
/* 236 */     this.settingButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/* 239 */         Gdx.app.log("Main", "Setting Button Pressed");
/* 240 */         GameScreen.this.game.setScreen(new MainMenuScreen(GameScreen.this.game));
/*     */       }
/*     */     });
/* 246 */     this.stage.addActor(this.playButton);
/* 247 */     this.stage.addActor(this.exitButton);
/* 248 */     this.stage.addActor(this.instructionButton);
/* 249 */     this.stage.addActor(this.settingButton);
/* 250 */     this.stage.setViewport(this.viewPort);
/*     */ 
/* 254 */     this.inputMultiplexer = new InputMultiplexer();
/* 255 */     this.inputMultiplexer.addProcessor(this.player1);
/* 256 */     this.inputMultiplexer.addProcessor(this.stage);
/* 257 */     Gdx.input.setInputProcessor(this.inputMultiplexer);
/*     */ 
/* 259 */     Gdx.app.log("Level " + this.level, "Level initialized");
/*     */   }
/*     */ 
/*     */   public void setCam() {
/* 263 */     this.cam = new OrthographicCamera();
/* 264 */     this.viewPort = new ScreenViewport(this.cam);
/* 265 */     this.cam.zoom = 0.01F;
/* 266 */     this.cam.position.set(this.player1.body.getPosition().x * 0.015625F / 2.0F + 30.0F, this.player1.body.getPosition().x * 0.015625F / 2.0F, 0.0F);
/* 267 */     this.cam.update();
/*     */   }
/*     */ 
/*     */   public void setMap()
/*     */   {
/* 272 */     MapBodyBuilder.buildShapes(this.map, 2, 64.0F, this.world);
/* 273 */     MapBodyBuilder.buildShapes(this.map, 3, 64.0F, this.world);
/* 274 */     MapBodyBuilder.buildShapes(this.map, 4, 64.0F, this.world);
/* 275 */     if (this.level >= 6)
/* 276 */       MapBodyBuilder.buildShapes(this.map, 5, 64.0F, this.world);
/*     */   }
/*     */ 
/*     */   public void addOrb(Color color, float x, float y)
/*     */   {
/* 281 */     this.orbs.add(new Orb(this, color, x, y));
/*     */   }
/*     */ 
/*     */   public void addMplat(float x1, float y1, float x2, float y2, Color color, float speed) {
/* 285 */     this.mplats.add(new MovingPlatform(this, x1, y1, x2, y2, color, speed));
/*     */   }
/*     */ 
/*     */   public void render(float delta)
/*     */   {
/* 293 */     if (this.state == State.Running)
/*     */     {
/* 295 */       this.cam.zoom = 0.01F;
/*     */ 
/* 297 */       Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 298 */       Gdx.gl.glClear(16384);
/*     */ 
/* 300 */       update(delta);
/*     */ 
/* 304 */       this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 306 */       this.renderer.setView(this.cam);
/* 307 */       this.renderer.render();
/*     */ 
/* 309 */       this.batch.begin();
/* 310 */       this.batch.end();
/*     */ 
/* 313 */       this.player1.render();
/*     */ 
/* 315 */       drawTextBehind();
/*     */ 
/* 317 */       if ((this.orbs != null) && (!this.orbs.isEmpty())) {
/* 318 */         for (int i = 0; i < this.orbs.size(); i++) {
/* 319 */           Orb orb = (Orb)this.orbs.get(i);
/* 320 */           orb.render();
/*     */         }
/*     */       }
/*     */ 
/* 324 */       if ((this.mplats != null) && (!this.mplats.isEmpty())) {
/* 325 */         for (int i = 0; i < this.mplats.size(); i++) {
/* 326 */           MovingPlatform mplat = (MovingPlatform)this.mplats.get(i);
/* 327 */           mplat.render();
/*     */         }
/*     */       }
/*     */ 
/* 331 */       this.rayHandler.setCombinedMatrix(this.cam.combined);
/* 332 */       this.rayHandler.render();
/*     */ 
/* 334 */       drawTextFront();
/*     */ 
/* 336 */       if (this.showDebug) {
/* 337 */         this.debugRenderer.render(this.world, this.cam.combined);
/*     */ 
/* 339 */         float x = Math.round(this.player1.body.getPosition().x * 100.0F) / 100.0F;
/* 340 */         float y = Math.round(this.player1.body.getPosition().y * 100.0F) / 100.0F;
/* 341 */         float velx = Math.round(this.player1.body.getLinearVelocity().x * 100.0F) / 100.0F;
/* 342 */         float vely = Math.round(this.player1.body.getLinearVelocity().y * 100.0F) / 100.0F;
/* 343 */         float light = Math.round(this.player1.playerLight.getDistance() / this.player1.lightLength * 100.0F * 100.0F) / 100.0F;
/*     */ 
/* 345 */         this.HUDbatch.begin();
/* 346 */         if (this.player1.godMode)
/* 347 */           this.font.draw(this.HUDbatch, "GHEF MODE ACTIVATED", 30.0F, 160.0F);
/* 348 */         this.font.draw(this.HUDbatch, "Player Stats:", 30.0F, 120.0F);
/* 349 */         this.font.draw(this.HUDbatch, "Position: " + x + " , " + y, 30.0F, 100.0F);
/* 350 */         this.font.draw(this.HUDbatch, "Velocity: " + velx + " , " + vely, 30.0F, 80.0F);
/* 351 */         this.font.draw(this.HUDbatch, "Light Energy: " + light + "%", 30.0F, 60.0F);
/* 352 */         this.font.draw(this.HUDbatch, "Jump count: " + this.player1.jumpCount, 30.0F, 40.0F);
/* 353 */         this.HUDbatch.end();
/*     */       }
/*     */     }
/*     */     else {
/* 357 */       this.cam.zoom = 1.0F;
/*     */ 
/* 359 */       Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 360 */       Gdx.gl.glClear(16384);
/*     */ 
/* 362 */       this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 364 */       this.stage.draw();
/* 365 */       this.stage.act();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void levelEnd()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void orbCollision()
/*     */   {
/* 446 */     if ((this.orbs != null) && (!this.orbs.isEmpty()))
/* 447 */       for (int i = 0; i < this.orbs.size(); i++) {
/* 448 */         Orb orb = (Orb)this.orbs.get(i);
/* 449 */         if (((Fixture)orb.body.getFixtureList().get(0)).getUserData() == "done") {
/* 450 */           orb.orbLight.setDistance(0.0F);
/* 451 */           ((Fixture)orb.body.getFixtureList().get(0)).setSensor(true);
/* 452 */           this.player1.setColor(orb.getColor());
/*     */         }
/*     */       }
/*     */   }
/*     */ 
/*     */   public void update(float delta)
/*     */   {
/* 460 */     if (this.state == State.Running)
/*     */     {
/* 462 */       if (this.playerDead) {
/* 463 */         playerDeath();
/*     */       }
/*     */ 
/* 466 */       this.world.step(0.01666667F, 6, 2);
/*     */ 
/* 468 */       float lerp = 0.8F;
/* 469 */       Vector3 position = this.cam.position;
/* 470 */       position.x += (this.player1.body.getPosition().x - position.x) * lerp * delta;
/* 471 */       position.y += (this.player1.body.getPosition().y - position.y) * lerp * delta;
/*     */ 
/* 473 */       if (this.player1.playerLight.getDistance() <= 0.3F) {
/* 474 */         this.playerDead = true;
/*     */       }
/*     */ 
/* 477 */       this.rayHandler.update();
/*     */ 
/* 480 */       this.cam.update();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void playerDeath()
/*     */   {
/* 490 */     this.player1.body.setTransform(this.startPosition, 0.0F);
/* 491 */     this.player1.body.setLinearVelocity(0.0F, 0.0F);
/* 492 */     this.player1.body.setAngularVelocity(0.0F);
/* 493 */     this.player1.jumpCount = 0;
/* 494 */     this.player1.canJump = true;
/*     */ 
/* 496 */     this.player1.playerLight.setDistance(this.player1.lightLength);
/*     */ 
/* 498 */     Vector3 position = this.cam.position;
/* 499 */     this.cam.position.x = this.player1.body.getPosition().x;
/* 500 */     this.playerDead = false;
/*     */   }
/*     */ 
/*     */   public void drawTextFront()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void drawTextBehind()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resize(int width, int height)
/*     */   {
/* 513 */     this.viewPort.update(width, height);
/*     */   }
/*     */ 
/*     */   public void pause()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resume()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void hide()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 533 */     Gdx.app.log("Level " + this.level, "Disposing");
/* 534 */     this.player1.dispose();
/* 535 */     this.world.dispose();
/*     */   }
/*     */ 
/*     */   public class ListenerClass
/*     */     implements ContactListener
/*     */   {
/*     */     public ListenerClass()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void endContact(Contact contact)
/*     */     {
/* 377 */       Fixture fixtureA = contact.getFixtureA();
/* 378 */       Fixture fixtureB = contact.getFixtureB();
/*     */ 
/* 380 */       if ((fixtureB.getBody().getUserData() == "ground") && (fixtureA.getBody().getUserData() == "sensor")) {
/* 381 */         GameScreen.this.player1.jumpCount -= 1;
/* 382 */         System.out.println("subtracted: " + GameScreen.this.player1.jumpCount);
/*     */       }
/* 384 */       else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "mplat")) {
/* 385 */         GameScreen.this.player1.jumpCount -= 1;
/* 386 */       } else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "trampoline")) {
/* 387 */         GameScreen.this.player1.jumpCount -= 1;
/*     */       }
/* 389 */       if (GameScreen.this.player1.jumpCount < 0)
/* 390 */         GameScreen.this.player1.jumpCount = 0;
/*     */     }
/*     */ 
/*     */     public void beginContact(Contact contact)
/*     */     {
/* 397 */       if (GameScreen.this.player1.hasJumped) {
/* 398 */         GameScreen.this.player1.jumpCount = 0;
/* 399 */         GameScreen.this.player1.hasJumped = false;
/*     */       }
/*     */ 
/* 402 */       Fixture fixtureA = contact.getFixtureA();
/* 403 */       Fixture fixtureB = contact.getFixtureB();
/*     */ 
/* 406 */       if ((fixtureB.getBody().getUserData() == "ground") && (fixtureA.getBody().getUserData() == "sensor")) {
/* 407 */         GameScreen.this.player1.jumpCount += 1;
/*     */       }
/* 409 */       else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "mplat"))
/* 410 */         GameScreen.this.player1.jumpCount += 1;
/* 411 */       else if ((fixtureB.getBody().getUserData() == "sensor") && (fixtureA.getBody().getUserData() == "trampoline")) {
/* 412 */         GameScreen.this.player1.jumpCount += 1;
/*     */       }
/* 414 */       if ((fixtureA.getBody().getUserData() == "spike") && (fixtureB.getBody().getUserData() == "player") && 
/* 415 */         (!GameScreen.this.player1.godMode)) {
/* 416 */         GameScreen.this.playerDead = true;
/*     */       }
/*     */ 
/* 419 */       if ((fixtureA.getBody().getUserData() == "end") && (fixtureB.getBody().getUserData() == "player")) {
/* 420 */         GameScreen.this.levelEnd();
/*     */       }
/*     */ 
/* 423 */       if ((fixtureB.getBody().getUserData() == "orb") && (fixtureA.getBody().getUserData() == "player") && (fixtureB.getUserData() != "done")) {
/* 424 */         GameScreen.this.player1.playerLight.setDistance(GameScreen.this.player1.playerLight.getDistance() + GameScreen.this.player1.lightLength);
/* 425 */         if (GameScreen.this.player1.playerLight.getDistance() >= 12.0F) {
/* 426 */           GameScreen.this.player1.playerLight.setDistance(12.0F);
/*     */         }
/* 428 */         fixtureB.setUserData("done");
/* 429 */         GameScreen.this.startPosition.x = fixtureB.getBody().getPosition().x;
/* 430 */         GameScreen.this.startPosition.y = (fixtureB.getBody().getPosition().y + 0.2F);
/* 431 */         GameScreen.this.orbCollision();
/*     */       }
/*     */     }
/*     */ 
/*     */     public void preSolve(Contact contact, Manifold oldManifold)
/*     */     {
/*     */     }
/*     */ 
/*     */     public void postSolve(Contact contact, ContactImpulse impulse)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static enum State
/*     */   {
/*  59 */     Running, Paused;
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.GameScreen
 * JD-Core Version:    0.6.2
 */