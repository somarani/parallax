/*     */ package com.compani.shooter.screens;
/*     */ 
/*     */ import com.badlogic.gdx.Application;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Input;
/*     */ import com.badlogic.gdx.Screen;
/*     */ import com.badlogic.gdx.graphics.GL20;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.BitmapFont;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureAtlas;
/*     */ import com.badlogic.gdx.math.Vector3;
/*     */ import com.badlogic.gdx.scenes.scene2d.InputEvent;
/*     */ import com.badlogic.gdx.scenes.scene2d.Stage;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.Skin;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
/*     */ import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
/*     */ import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
/*     */ import com.badlogic.gdx.utils.viewport.FillViewport;
/*     */ import com.badlogic.gdx.utils.viewport.Viewport;
/*     */ 
/*     */ public class MainMenuScreen
/*     */   implements Screen
/*     */ {
/*     */   Game game;
/*     */   Stage stage;
/*     */   TextButton playButton;
/*     */   TextButton exitButton;
/*     */   TextButton settingButton;
/*     */   TextButton instructionButton;
/*     */   TextButton.TextButtonStyle textButtonStyle;
/*     */   BitmapFont font;
/*     */   Skin skin;
/*     */   TextureAtlas buttonAtlas;
/*     */   SpriteBatch batch;
/*     */   Texture background;
/*     */   OrthographicCamera cam;
/*     */   Viewport viewPort;
/*     */ 
/*     */   public MainMenuScreen(Game g)
/*     */   {
/*  55 */     this.game = g;
/*     */   }
/*     */ 
/*     */   public void show()
/*     */   {
/*  61 */     Gdx.app.log("Main", "Main Menu loaded");
/*     */ 
/*  64 */     this.cam = new OrthographicCamera();
/*  65 */     this.viewPort = new FillViewport(1920.0F, 1080.0F, this.cam);
/*  66 */     this.cam.position.set(this.viewPort.getWorldWidth() / 2.0F, this.viewPort.getWorldHeight() / 2.0F, 0.0F);
/*  67 */     this.cam.update();
/*     */ 
/*  70 */     this.stage = new Stage();
/*  71 */     this.font = new BitmapFont(Gdx.files.local("font.fnt"));
/*  72 */     this.skin = new Skin();
/*  73 */     this.buttonAtlas = new TextureAtlas("buttons.pack");
/*  74 */     this.skin.addRegions(this.buttonAtlas);
/*  75 */     this.batch = new SpriteBatch();
/*  76 */     this.background = new Texture("mainbg.png");
/*     */ 
/*  78 */     Gdx.input.setInputProcessor(this.stage);
/*     */ 
/*  81 */     this.textButtonStyle = new TextButton.TextButtonStyle();
/*  82 */     this.textButtonStyle.font = this.font;
/*  83 */     this.textButtonStyle.up = this.skin.getDrawable("unclicked");
/*  84 */     this.textButtonStyle.over = this.skin.getDrawable("hovered");
/*  85 */     this.textButtonStyle.down = this.skin.getDrawable("clicked");
/*  86 */     this.textButtonStyle.font.setColor(93.0F, 93.0F, 93.0F, 1.0F);
/*     */ 
/*  89 */     this.playButton = new TextButton("Play", this.textButtonStyle);
/*  90 */     this.exitButton = new TextButton("Exit", this.textButtonStyle);
/*  91 */     this.instructionButton = new TextButton("Instructions", this.textButtonStyle);
/*  92 */     this.settingButton = new TextButton("Settings", this.textButtonStyle);
/*     */ 
/*  95 */     this.playButton.setX(675.0F);
/*  96 */     this.playButton.setY(300.0F);
/*     */ 
/*  98 */     this.instructionButton.setX(975.0F);
/*  99 */     this.instructionButton.setY(300.0F);
/*     */ 
/* 101 */     this.settingButton.setX(675.0F);
/* 102 */     this.settingButton.setY(160.0F);
/*     */ 
/* 104 */     this.exitButton.setX(975.0F);
/* 105 */     this.exitButton.setY(160.0F);
/*     */ 
/* 108 */     this.playButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 112 */         MainMenuScreen.this.game.setScreen(new LevelSelectScreen(MainMenuScreen.this.game));
/* 113 */         Gdx.app.log("Main", "Play Button Pressed");
/*     */       }
/*     */     });
/* 119 */     this.instructionButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y)
/*     */       {
/* 123 */         MainMenuScreen.this.game.setScreen(new InstructionsScreen(MainMenuScreen.this.game));
/* 124 */         Gdx.app.log("Main", "Instruction Button Pressed");
/*     */       }
/*     */     });
/* 130 */     this.exitButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/* 133 */         Gdx.app.log("Main", "Exit Button Pressed");
/* 134 */         Gdx.app.exit();
/*     */       }
/*     */     });
/* 139 */     this.settingButton.addListener(new ClickListener()
/*     */     {
/*     */       public void clicked(InputEvent event, float x, float y) {
/* 142 */         Gdx.app.log("Main", "Setting Button Pressed");
/* 143 */         MainMenuScreen.this.game.setScreen(new SettingsScreen(MainMenuScreen.this.game));
/*     */       }
/*     */     });
/* 149 */     this.stage.addActor(this.playButton);
/* 150 */     this.stage.addActor(this.exitButton);
/* 151 */     this.stage.addActor(this.instructionButton);
/* 152 */     this.stage.addActor(this.settingButton);
/* 153 */     this.stage.setViewport(this.viewPort);
/*     */   }
/*     */ 
/*     */   public void render(float delta)
/*     */   {
/* 159 */     Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
/* 160 */     Gdx.gl.glClear(16384);
/*     */ 
/* 162 */     this.batch.setProjectionMatrix(this.cam.combined);
/*     */ 
/* 164 */     this.batch.begin();
/* 165 */     this.batch.draw(this.background, 0.0F, 0.0F, 1920.0F, 1080.0F);
/* 166 */     this.batch.end();
/*     */ 
/* 168 */     this.stage.draw();
/* 169 */     this.stage.act();
/*     */   }
/*     */ 
/*     */   public void resize(int width, int height)
/*     */   {
/* 174 */     this.viewPort.update(width, height);
/*     */   }
/*     */ 
/*     */   public void pause()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resume()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void hide()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 194 */     this.stage.dispose();
/* 195 */     this.background.dispose();
/* 196 */     this.batch.dispose();
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.screens.MainMenuScreen
 * JD-Core Version:    0.6.2
 */