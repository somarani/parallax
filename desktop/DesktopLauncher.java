/*    */ package com.compani.shooter.desktop;
/*    */ 
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
/*    */ import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
/*    */ import com.compani.shooter.main.Constants;
/*    */ import com.compani.shooter.main.Main;
/*    */ 
/*    */ public class DesktopLauncher
/*    */ {
/*    */   public static void main(String[] arg)
/*    */   {
/* 14 */     LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
/* 15 */     config.width = Constants.WIDTH;
/* 16 */     config.height = Constants.HEIGHT;
/* 17 */     config.title = "Parallax";
/* 18 */     config.fullscreen = true;
/* 19 */     config.samples = 0;
/*    */ 
/* 21 */     new LwjglApplication(new Main(), config);
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.desktop.DesktopLauncher
 * JD-Core Version:    0.6.2
 */