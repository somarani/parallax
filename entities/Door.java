/*     */ package com.compani.shooter.entities;
/*     */ 
/*     */ import box2dLight.PointLight;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*     */ import com.badlogic.gdx.physics.box2d.CircleShape;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.FixtureDef;
/*     */ import com.badlogic.gdx.physics.box2d.PolygonShape;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.compani.shooter.screens.GameScreen;
/*     */ 
/*     */ public class Door
/*     */ {
/*     */   GameScreen screen;
/*     */   World world;
/*  22 */   boolean doorOpen = false;
/*  23 */   public boolean isDone = false;
/*     */   float x1;
/*     */   float x2;
/*     */   float y1;
/*     */   float y2;
/*     */   SpriteBatch batch;
/*     */   Texture doorTexture;
/*     */   Texture keyTexture;
/*     */   public PointLight keyLight;
/*     */   public Body body;
/*     */   BodyDef def;
/*     */   FixtureDef fixtureDef;
/*     */   public Body keyBody;
/*     */   BodyDef keyDef;
/*     */   FixtureDef keyFixtureDef;
/*     */   public PointLight light;
/*     */   Color color;
/*     */ 
/*     */   public Door(GameScreen screen, float x1, float y1, float x2, float y2, Color color)
/*     */   {
/*  47 */     this.screen = screen;
/*  48 */     this.world = screen.world;
/*  49 */     this.x1 = x1;
/*  50 */     this.x2 = x2;
/*  51 */     this.y1 = y1;
/*  52 */     this.y2 = y2;
/*  53 */     this.color = color;
/*     */ 
/*  55 */     definePlatform();
/*  56 */     defineKey();
/*     */ 
/*  58 */     this.body.setTransform(x1, y1, 0.0F);
/*  59 */     this.keyBody.setTransform(x2, y2, 0.0F);
/*     */ 
/*  61 */     this.batch = new SpriteBatch();
/*  62 */     this.keyTexture = new Texture(Gdx.files.local("key.png"));
/*  63 */     this.doorTexture = new Texture(Gdx.files.local("door.png"));
/*     */   }
/*     */ 
/*     */   public void definePlatform()
/*     */   {
/*  69 */     this.def = new BodyDef();
/*  70 */     this.def.type = BodyDef.BodyType.KinematicBody;
/*  71 */     this.def.position.set(128.0F, 128.0F);
/*     */ 
/*  73 */     this.body = this.world.createBody(this.def);
/*     */ 
/*  75 */     this.body.setUserData("door");
/*     */ 
/*  77 */     PolygonShape shape = new PolygonShape();
/*  78 */     Vector2 size = new Vector2(1.0F, 1.0F);
/*  79 */     shape.setAsBox(0.5F, 0.5F, size, 0.0F);
/*  80 */     this.fixtureDef = new FixtureDef();
/*  81 */     this.fixtureDef.shape = shape;
/*  82 */     this.fixtureDef.restitution = 0.0F;
/*  83 */     this.fixtureDef.density = 1.0F;
/*     */ 
/*  85 */     this.body.setGravityScale(0.0F);
/*     */ 
/*  87 */     Fixture fixutre = this.body.createFixture(this.fixtureDef);
/*     */ 
/*  89 */     shape.dispose();
/*     */ 
/*  91 */     this.light = new PointLight(this.screen.rayHandler, 5, this.color, 1.0F, 0.0F, 0.0F);
/*  92 */     this.light.setXray(true);
/*  93 */     this.light.setSoftnessLength(1.0F);
/*  94 */     this.light.attachToBody(this.body, 1.0F, 1.0F);
/*     */   }
/*     */ 
/*     */   public void defineKey()
/*     */   {
/* 100 */     this.keyDef = new BodyDef();
/* 101 */     this.keyDef.type = BodyDef.BodyType.DynamicBody;
/* 102 */     this.keyDef.position.set(128.0F, 128.0F);
/* 103 */     this.keyBody = this.world.createBody(this.keyDef);
/* 104 */     CircleShape shape = new CircleShape();
/* 105 */     shape.setRadius(0.3125F);
/* 106 */     this.keyFixtureDef = new FixtureDef();
/* 107 */     this.keyFixtureDef.shape = shape;
/* 108 */     this.keyBody.setUserData("key");
/* 109 */     this.keyFixtureDef.restitution = 0.0F;
/* 110 */     this.keyFixtureDef.density = 0.2F;
/* 111 */     this.keyBody.setGravityScale(0.0F);
/* 112 */     Fixture fixture = this.keyBody.createFixture(this.keyFixtureDef);
/* 113 */     shape.dispose();
/*     */ 
/* 116 */     this.keyLight = new PointLight(this.screen.rayHandler, 200, this.color, 0.5F, 0.0F, 0.0F);
/* 117 */     this.keyLight.setXray(true);
/* 118 */     this.keyLight.setSoftnessLength(0.1F);
/* 119 */     this.keyLight.attachToBody(this.keyBody);
/*     */   }
/*     */ 
/*     */   public void update() {
/* 123 */     if (this.keyBody.getPosition().x != this.x2) {
/* 124 */       this.doorOpen = true;
/*     */     }
/* 126 */     if (this.doorOpen)
/*     */     {
/* 128 */       this.light.setDistance(this.light.getDistance() - 0.03F);
/*     */ 
/* 130 */       if (Math.abs(this.body.getPosition().y - this.y1) > 1.0F) {
/* 131 */         this.body.setTransform(0.0F, -3.0F, 0.0F);
/* 132 */         this.body.setActive(false);
/*     */ 
/* 134 */         this.isDone = true;
/*     */       }
/*     */ 
/* 137 */       this.body.setLinearVelocity(0.0F, -1.0F);
/* 138 */       this.keyLight.setDistance(0.0F);
/* 139 */       this.keyBody.setTransform(0.0F, -3.0F, 0.0F);
/* 140 */       this.keyBody.setActive(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void render()
/*     */   {
/* 146 */     update();
/*     */ 
/* 148 */     this.batch.setProjectionMatrix(this.screen.cam.combined);
/* 149 */     this.batch.begin();
/* 150 */     this.batch.draw(this.doorTexture, this.body.getPosition().x + 0.5F, this.body.getPosition().y + 0.5F, 1.0F, 1.0F);
/* 151 */     if (!this.doorOpen)
/* 152 */       this.batch.draw(this.keyTexture, this.keyBody.getPosition().x - 0.5F, this.keyBody.getPosition().y - 0.460938F, 1.0F, 1.0F);
/* 153 */     this.batch.end();
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.entities.Door
 * JD-Core Version:    0.6.2
 */