/*     */ package com.compani.shooter.entities;
/*     */ 
/*     */ import box2dLight.PointLight;
/*     */ import box2dLight.RayHandler;
/*     */ import com.badlogic.gdx.Game;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.Graphics;
/*     */ import com.badlogic.gdx.InputProcessor;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
/*     */ import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*     */ import com.badlogic.gdx.physics.box2d.CircleShape;
/*     */ import com.badlogic.gdx.physics.box2d.Filter;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.FixtureDef;
/*     */ import com.badlogic.gdx.physics.box2d.PolygonShape;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.compani.shooter.main.Constants;
/*     */ import com.compani.shooter.screens.GameScreen;
/*     */ import com.compani.shooter.screens.GameScreen.State;
/*     */ import com.compani.shooter.screens.MainMenuScreen;
/*     */ import com.compani.shooter.screens.levels.Level1;
/*     */ import com.compani.shooter.screens.levels.Level10;
/*     */ import com.compani.shooter.screens.levels.Level2;
/*     */ import com.compani.shooter.screens.levels.Level3;
/*     */ import com.compani.shooter.screens.levels.Level4;
/*     */ import com.compani.shooter.screens.levels.Level5;
/*     */ import com.compani.shooter.screens.levels.Level6;
/*     */ import com.compani.shooter.screens.levels.Level7;
/*     */ import com.compani.shooter.screens.levels.Level8;
/*     */ import com.compani.shooter.screens.levels.Level9;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ public class Player
/*     */   implements InputProcessor
/*     */ {
/*     */   private Color color;
/*     */   public ShapeRenderer shapeRenderer;
/*     */   private int[] keyMap;
/*     */   private boolean[] keysPressed;
/*  34 */   public int jumpCount = 0;
/*     */ 
/*  36 */   public boolean godMode = false;
/*     */   public PointLight playerLight;
/*  40 */   public float lightLength = 8.0F;
/*     */ 
/*  42 */   public boolean hasJumped = false;
/*     */   private GameScreen screen;
/*     */   public World world;
/*     */   public Body body;
/*     */   BodyDef def;
/*     */   FixtureDef fixtureDef;
/*     */   public Body body1;
/*     */   BodyDef def1;
/*     */   FixtureDef fixtureDef1;
/*     */   public State currentState;
/*  58 */   public boolean canJump = true;
/*     */ 
/*     */   public Player(GameScreen screen, Color color, int[] keyMap) {
/*  61 */     this.shapeRenderer = new ShapeRenderer();
/*  62 */     this.color = color;
/*  63 */     this.keyMap = keyMap;
/*  64 */     this.screen = screen;
/*  65 */     this.world = screen.world;
/*  66 */     this.keysPressed = new boolean[5];
/*  67 */     this.currentState = State.STANDING;
/*  68 */     definePlayer();
/*     */ 
/*  70 */     keyMap = Constants.player1keys;
/*     */   }
/*     */ 
/*     */   public void update(float delta)
/*     */   {
/*  75 */     if ((this.keysPressed[0] != 0) && (Math.abs(this.body.getLinearVelocity().x) <= 4.0F))
/*     */     {
/*  77 */       if (this.godMode) {
/*  78 */         this.body.setTransform(this.body.getPosition().x + 0.1F, this.body.getPosition().y, 0.0F);
/*     */       }
/*  80 */       else if ((this.currentState != State.FALLING) && (this.currentState != State.JUMPING))
/*  81 */         this.body.applyLinearImpulse(0.08F, 0.0F, this.body.getPosition().x, this.body.getPosition().y, true);
/*     */       else {
/*  83 */         this.body.applyLinearImpulse(0.06F, 0.0F, this.body.getPosition().x, this.body.getPosition().y, true);
/*     */       }
/*     */     }
/*  86 */     if ((this.keysPressed[1] != 0) && (Math.abs(this.body.getLinearVelocity().x) <= 4.0F))
/*     */     {
/*  88 */       if (this.godMode) {
/*  89 */         this.body.setTransform(this.body.getPosition().x - 0.1F, this.body.getPosition().y, 0.0F);
/*     */       }
/*  91 */       else if ((this.currentState != State.FALLING) && (this.currentState != State.JUMPING))
/*  92 */         this.body.applyLinearImpulse(-0.08F, 0.0F, this.body.getPosition().x, this.body.getPosition().y, true);
/*     */       else {
/*  94 */         this.body.applyLinearImpulse(-0.06F, 0.0F, this.body.getPosition().x, this.body.getPosition().y, true);
/*     */       }
/*     */     }
/*     */ 
/*  98 */     if (this.keysPressed[2] != 0)
/*     */     {
/* 100 */       if (this.godMode) {
/* 101 */         this.body.setTransform(this.body.getPosition().x, this.body.getPosition().y + 0.1F, 0.0F);
/*     */       }
/* 103 */       else if ((this.jumpCount > 0) && (!this.hasJumped)) {
/* 104 */         System.out.println("before jump: " + this.body.getLinearVelocity());
/* 105 */         float current = this.body.getLinearVelocity().y;
/* 106 */         current = current + 6.5D < 8.0D ? current : 0.0F;
/* 107 */         this.body.setLinearVelocity(this.body.getLinearVelocity().x, current + 6.5F);
/* 108 */         this.hasJumped = true;
/* 109 */         System.out.println("after jump: " + this.body.getLinearVelocity());
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 114 */     if ((this.godMode) && (this.keysPressed[3] != 0)) {
/* 115 */       this.body.setTransform(this.body.getPosition().x, this.body.getPosition().y - 0.1F, 0.0F);
/*     */     }
/*     */ 
/* 118 */     if ((this.keysPressed[1] == 0) && (this.keysPressed[0] == 0) && (Math.abs(this.body.getLinearVelocity().x) > 0.0F)) {
/* 119 */       this.body.setAngularVelocity(0.0F);
/*     */     }
/* 121 */     this.currentState = getState();
/*     */ 
/* 123 */     this.body1.setTransform(this.body.getPosition().x, this.body.getPosition().y - 0.4375F, 0.0F);
/*     */   }
/*     */ 
/*     */   public void stop() {
/* 127 */     this.body.setLinearVelocity(0.0F, 0.0F);
/* 128 */     this.body.setAngularVelocity(0.0F);
/* 129 */     this.currentState = State.STANDING;
/*     */   }
/*     */ 
/*     */   public void godMode() {
/* 133 */     this.godMode = (!this.godMode);
/*     */ 
/* 135 */     if (this.godMode) {
/* 136 */       this.body.setLinearVelocity(0.0F, 0.0F);
/* 137 */       this.body.setGravityScale(0.0F);
/* 138 */       this.screen.showDebug = true;
/* 139 */       this.playerLight.setDistance(this.lightLength);
/* 140 */       this.playerLight.setColor(Color.WHITE);
/*     */     }
/*     */     else {
/* 143 */       this.body.setGravityScale(1.0F);
/* 144 */       this.jumpCount = 0;
/*     */     }
/*     */   }
/*     */ 
/*     */   public State getState() {
/* 149 */     if (this.body.getLinearVelocity().y < 0.0F)
/* 150 */       return State.FALLING;
/* 151 */     if (this.body.getLinearVelocity().y > 0.0F)
/* 152 */       return State.JUMPING;
/* 153 */     if (this.body.getLinearVelocity().x > 0.0F)
/* 154 */       return State.MOVING_RIGHT;
/* 155 */     if (this.body.getLinearVelocity().x < 0.0F) {
/* 156 */       return State.MOVING_LEFT;
/*     */     }
/* 158 */     return State.STANDING;
/*     */   }
/*     */ 
/*     */   public void render() {
/* 162 */     update(Gdx.graphics.getDeltaTime());
/*     */ 
/* 164 */     this.shapeRenderer.setProjectionMatrix(this.screen.cam.combined);
/* 165 */     this.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
/* 166 */     this.shapeRenderer.setColor(Color.GRAY);
/* 167 */     this.shapeRenderer.circle(this.body.getPosition().x, this.body.getPosition().y, 0.4375F, 50);
/* 168 */     this.shapeRenderer.end();
/*     */   }
/*     */ 
/*     */   public void setColor(Color color)
/*     */   {
/* 173 */     this.playerLight.setColor(color);
/*     */   }
/*     */ 
/*     */   public void definePlayer()
/*     */   {
/* 179 */     this.def = new BodyDef();
/* 180 */     this.def.type = BodyDef.BodyType.DynamicBody;
/* 181 */     this.def.position.set(128.0F, 128.0F);
/* 182 */     this.body = this.world.createBody(this.def);
/* 183 */     CircleShape shape = new CircleShape();
/* 184 */     shape.setRadius(0.4375F);
/* 185 */     this.fixtureDef = new FixtureDef();
/* 186 */     this.fixtureDef.shape = shape;
/* 187 */     this.body.setUserData("player");
/* 188 */     this.fixtureDef.restitution = 0.0F;
/* 189 */     this.fixtureDef.density = 1.0F;
/* 190 */     Fixture fixture = this.body.createFixture(this.fixtureDef);
/* 191 */     shape.dispose();
/*     */ 
/* 195 */     this.def1 = new BodyDef();
/* 196 */     this.def1.type = BodyDef.BodyType.DynamicBody;
/* 197 */     this.def1.position.set(128.0F, 128.0F);
/* 198 */     this.body1 = this.world.createBody(this.def);
/* 199 */     PolygonShape rect = new PolygonShape();
/* 200 */     rect.setAsBox(0.21875F, 0.015625F);
/* 201 */     this.fixtureDef1 = new FixtureDef();
/* 202 */     this.fixtureDef1.shape = rect;
/* 203 */     this.fixtureDef1.filter.groupIndex = -1;
/* 204 */     this.body1.setUserData("sensor");
/* 205 */     Fixture fixture1 = this.body1.createFixture(this.fixtureDef1);
/* 206 */     fixture1.setSensor(true);
/* 207 */     rect.dispose();
/*     */ 
/* 218 */     this.playerLight = new PointLight(this.screen.rayHandler, 200, this.color, this.lightLength, 0.0F, 0.0F);
/* 219 */     this.playerLight.setXray(false);
/* 220 */     this.playerLight.setSoftnessLength(3.0F);
/* 221 */     this.playerLight.attachToBody(this.body);
/*     */ 
/* 224 */     Filter a = new Filter();
/* 225 */     a.maskBits = 1;
/* 226 */     a.groupIndex = -1;
/* 227 */     a.categoryBits = 1;
/*     */ 
/* 229 */     this.playerLight.setContactFilter(a);
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/* 234 */     this.screen.rayHandler.dispose();
/* 235 */     this.playerLight.dispose();
/*     */   }
/*     */ 
/*     */   public boolean keyDown(int keycode)
/*     */   {
/* 240 */     if (keycode == this.keyMap[0]) {
/* 241 */       this.keysPressed[0] = true;
/*     */     }
/* 243 */     if (keycode == this.keyMap[1]) {
/* 244 */       this.keysPressed[1] = true;
/*     */     }
/* 246 */     if (keycode == this.keyMap[2]) {
/* 247 */       this.keysPressed[2] = true;
/*     */     }
/* 249 */     if (keycode == this.keyMap[3]) {
/* 250 */       this.keysPressed[3] = true;
/*     */     }
/* 263 */     else if (keycode == 131) {
/* 264 */       if (this.screen.level == 0)
/* 265 */         this.screen.game.setScreen(new MainMenuScreen(this.screen.game));
/*     */       else
/* 267 */         this.screen.state = (this.screen.state == GameScreen.State.Running ? GameScreen.State.Paused : GameScreen.State.Running);
/*     */     }
/* 269 */     else if (keycode == 8)
/* 270 */       this.screen.game.setScreen(new Level1(this.screen.game));
/* 271 */     else if (keycode == 9)
/* 272 */       this.screen.game.setScreen(new Level2(this.screen.game));
/* 273 */     else if (keycode == 10)
/* 274 */       this.screen.game.setScreen(new Level3(this.screen.game));
/* 275 */     else if (keycode == 11)
/* 276 */       this.screen.game.setScreen(new Level4(this.screen.game));
/* 277 */     else if (keycode == 12)
/* 278 */       this.screen.game.setScreen(new Level5(this.screen.game));
/* 279 */     else if (keycode == 13)
/* 280 */       this.screen.game.setScreen(new Level6(this.screen.game));
/* 281 */     else if (keycode == 14)
/* 282 */       this.screen.game.setScreen(new Level7(this.screen.game));
/* 283 */     else if (keycode == 15)
/* 284 */       this.screen.game.setScreen(new Level8(this.screen.game));
/* 285 */     else if (keycode == 16)
/* 286 */       this.screen.game.setScreen(new Level9(this.screen.game));
/* 287 */     else if (keycode == 7) {
/* 288 */       this.screen.game.setScreen(new Level10(this.screen.game));
/*     */     }
/* 290 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean keyUp(int keycode)
/*     */   {
/* 295 */     if (keycode == this.keyMap[0]) {
/* 296 */       this.keysPressed[0] = false;
/*     */     }
/* 298 */     if (keycode == this.keyMap[1]) {
/* 299 */       this.keysPressed[1] = false;
/*     */     }
/* 301 */     if (keycode == this.keyMap[2]) {
/* 302 */       this.keysPressed[2] = false;
/*     */     }
/* 304 */     if (keycode == this.keyMap[3]) {
/* 305 */       this.keysPressed[3] = false;
/*     */     }
/* 307 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean keyTyped(char character)
/*     */   {
/* 312 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean touchDown(int screenX, int screenY, int pointer, int button)
/*     */   {
/* 317 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean touchUp(int screenX, int screenY, int pointer, int button)
/*     */   {
/* 322 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean touchDragged(int screenX, int screenY, int pointer)
/*     */   {
/* 327 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean mouseMoved(int screenX, int screenY)
/*     */   {
/* 332 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean scrolled(int amount)
/*     */   {
/* 337 */     return false;
/*     */   }
/*     */ 
/*     */   public static enum State
/*     */   {
/*  55 */     FALLING, JUMPING, STANDING, MOVING_RIGHT, MOVING_LEFT;
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.entities.Player
 * JD-Core Version:    0.6.2
 */