/*    */ package com.compani.shooter.entities;
/*    */ 
/*    */ import box2dLight.PointLight;
/*    */ import com.badlogic.gdx.Files;
/*    */ import com.badlogic.gdx.Gdx;
/*    */ import com.badlogic.gdx.graphics.Color;
/*    */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*    */ import com.badlogic.gdx.graphics.Texture;
/*    */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*    */ import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
/*    */ import com.badlogic.gdx.math.Vector2;
/*    */ import com.badlogic.gdx.physics.box2d.Body;
/*    */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*    */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*    */ import com.badlogic.gdx.physics.box2d.CircleShape;
/*    */ import com.badlogic.gdx.physics.box2d.Fixture;
/*    */ import com.badlogic.gdx.physics.box2d.FixtureDef;
/*    */ import com.badlogic.gdx.physics.box2d.World;
/*    */ import com.compani.shooter.screens.GameScreen;
/*    */ 
/*    */ public class Orb
/*    */ {
/*    */   GameScreen screen;
/*    */   Color color;
/*    */   World world;
/*    */   public ShapeRenderer shapeRenderer;
/*    */   SpriteBatch batch;
/*    */   Texture textureAlive;
/*    */   Texture textureDead;
/*    */   public Body body;
/*    */   BodyDef def;
/*    */   FixtureDef fixtureDef;
/*    */   public PointLight orbLight;
/*    */ 
/*    */   public Orb(GameScreen screen, Color color, float x, float y)
/*    */   {
/* 39 */     this.shapeRenderer = new ShapeRenderer();
/* 40 */     this.color = color;
/* 41 */     this.screen = screen;
/* 42 */     this.world = screen.world;
/* 43 */     defineOrb();
/*    */ 
/* 45 */     this.body.setTransform(x, y, 0.0F);
/*    */ 
/* 47 */     this.batch = new SpriteBatch();
/* 48 */     this.textureAlive = new Texture(Gdx.files.local("orb.png"));
/* 49 */     this.textureDead = new Texture(Gdx.files.local("orb.png"));
/*    */   }
/*    */ 
/*    */   public Color getColor()
/*    */   {
/* 54 */     return this.color;
/*    */   }
/*    */ 
/*    */   public void defineOrb()
/*    */   {
/* 60 */     this.def = new BodyDef();
/* 61 */     this.def.type = BodyDef.BodyType.DynamicBody;
/* 62 */     this.def.position.set(128.0F, 128.0F);
/* 63 */     this.body = this.world.createBody(this.def);
/* 64 */     CircleShape shape = new CircleShape();
/* 65 */     shape.setRadius(0.3125F);
/* 66 */     this.fixtureDef = new FixtureDef();
/* 67 */     this.fixtureDef.shape = shape;
/* 68 */     this.body.setUserData("orb");
/* 69 */     this.fixtureDef.restitution = 0.0F;
/* 70 */     this.fixtureDef.density = 1.0F;
/* 71 */     this.fixtureDef.filter.groupIndex = -1;
/* 72 */     this.body.setAngularDamping(20.0F);
/* 73 */     this.body.setGravityScale(0.0F);
/* 74 */     Fixture fixture = this.body.createFixture(this.fixtureDef);
/* 75 */     shape.dispose();
/*    */ 
/* 78 */     this.orbLight = new PointLight(this.screen.rayHandler, 200, this.color, 4.0F, 0.0F, 0.0F);
/* 79 */     this.orbLight.setXray(true);
/* 80 */     this.orbLight.setSoftnessLength(0.1F);
/* 81 */     this.orbLight.attachToBody(this.body);
/*    */   }
/*    */ 
/*    */   public void render()
/*    */   {
/* 93 */     this.batch.setProjectionMatrix(this.screen.cam.combined);
/* 94 */     this.batch.begin();
/* 95 */     if (this.body.getUserData() == "done")
/* 96 */       this.batch.draw(this.textureDead, this.body.getPosition().x - 0.5F, this.body.getPosition().y - 0.460938F, 1.0F, 1.0F);
/*    */     else
/* 98 */       this.batch.draw(this.textureAlive, this.body.getPosition().x - 0.5F, this.body.getPosition().y - 0.460938F, 1.0F, 1.0F);
/* 99 */     this.batch.end();
/*    */   }
/*    */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.entities.Orb
 * JD-Core Version:    0.6.2
 */