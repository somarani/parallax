/*     */ package com.compani.shooter.entities;
/*     */ 
/*     */ import box2dLight.PointLight;
/*     */ import com.badlogic.gdx.Files;
/*     */ import com.badlogic.gdx.Gdx;
/*     */ import com.badlogic.gdx.graphics.Color;
/*     */ import com.badlogic.gdx.graphics.OrthographicCamera;
/*     */ import com.badlogic.gdx.graphics.Texture;
/*     */ import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/*     */ import com.badlogic.gdx.graphics.g2d.TextureRegion;
/*     */ import com.badlogic.gdx.math.Vector2;
/*     */ import com.badlogic.gdx.physics.box2d.Body;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef;
/*     */ import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
/*     */ import com.badlogic.gdx.physics.box2d.Fixture;
/*     */ import com.badlogic.gdx.physics.box2d.FixtureDef;
/*     */ import com.badlogic.gdx.physics.box2d.PolygonShape;
/*     */ import com.badlogic.gdx.physics.box2d.World;
/*     */ import com.compani.shooter.screens.GameScreen;
/*     */ 
/*     */ public class MovingPlatform
/*     */ {
/*     */   GameScreen screen;
/*     */   World world;
/*     */   float x1;
/*     */   float x2;
/*     */   float y1;
/*     */   float y2;
/*     */   SpriteBatch batch;
/*     */   Texture texture;
/*     */   TextureRegion textureRegion;
/*     */   float speed;
/*     */   Vector2 directionVector;
/*  36 */   boolean direction = true;
/*     */   public Body body;
/*     */   BodyDef def;
/*     */   FixtureDef fixtureDef;
/*     */   public PointLight light;
/*     */   Color color;
/*     */ 
/*     */   public MovingPlatform(GameScreen screen, float x1, float y1, float x2, float y2, Color color, float speed)
/*     */   {
/*  47 */     this.screen = screen;
/*  48 */     this.world = screen.world;
/*  49 */     this.x1 = x1;
/*  50 */     this.x2 = x2;
/*  51 */     this.y1 = y1;
/*  52 */     this.y2 = y2;
/*  53 */     this.color = color;
/*     */ 
/*  55 */     this.speed = speed;
/*     */ 
/*  57 */     definePlatform();
/*     */ 
/*  59 */     this.body.setTransform(x1, y1, 0.0F);
/*     */ 
/*  61 */     this.directionVector = new Vector2((x2 - x1) * speed, (y2 - y1) * speed);
/*     */ 
/*  63 */     this.batch = new SpriteBatch();
/*  64 */     this.texture = new Texture(Gdx.files.local("mplat.png"));
/*  65 */     this.textureRegion = new TextureRegion(this.texture, 0, 0, 64, 64);
/*     */   }
/*     */ 
/*     */   public void definePlatform()
/*     */   {
/*  71 */     this.def = new BodyDef();
/*  72 */     this.def.type = BodyDef.BodyType.KinematicBody;
/*  73 */     this.def.position.set(128.0F, 128.0F);
/*     */ 
/*  75 */     this.body = this.world.createBody(this.def);
/*     */ 
/*  77 */     this.body.setUserData("ground");
/*     */ 
/*  79 */     PolygonShape shape = new PolygonShape();
/*  80 */     Vector2 size = new Vector2(1.0F, 1.0F);
/*  81 */     shape.setAsBox(0.5F, 0.5F, size, 0.0F);
/*  82 */     this.fixtureDef = new FixtureDef();
/*  83 */     this.fixtureDef.shape = shape;
/*  84 */     this.fixtureDef.restitution = 0.0F;
/*  85 */     this.fixtureDef.density = 1.0F;
/*     */ 
/*  87 */     this.body.setGravityScale(0.0F);
/*     */ 
/*  89 */     Fixture fixutre = this.body.createFixture(this.fixtureDef);
/*     */ 
/*  91 */     shape.dispose();
/*     */ 
/*  93 */     this.light = new PointLight(this.screen.rayHandler, 5, this.color, 1.5F, 0.0F, 0.0F);
/*  94 */     this.light.setXray(true);
/*  95 */     this.light.setSoftnessLength(1.0F);
/*  96 */     this.light.attachToBody(this.body, 1.0F, 1.0F);
/*     */   }
/*     */ 
/*     */   public void update()
/*     */   {
/* 102 */     if (this.direction) {
/* 103 */       this.body.setLinearVelocity(this.directionVector);
/*     */     }
/*     */     else {
/* 106 */       this.body.setLinearVelocity(this.directionVector.x * -1.0F, this.directionVector.y * -1.0F);
/*     */     }
/*     */ 
/* 109 */     if ((Math.abs(this.body.getPosition().x - this.x2) < 0.1D) && (Math.abs(this.body.getPosition().y - this.y2) < 0.1D)) {
/* 110 */       this.direction = false;
/*     */     }
/* 112 */     if ((Math.abs(this.body.getPosition().x - this.x1) < 0.1D) && (Math.abs(this.body.getPosition().y - this.y1) < 0.1D))
/* 113 */       this.direction = true;
/*     */   }
/*     */ 
/*     */   public void render()
/*     */   {
/* 118 */     update();
/*     */ 
/* 120 */     this.batch.setProjectionMatrix(this.screen.cam.combined);
/* 121 */     this.batch.begin();
/* 122 */     this.batch.draw(this.textureRegion, this.body.getPosition().x + 0.5F, this.body.getPosition().y + 0.5F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, this.body.getAngle());
/* 123 */     this.batch.end();
/*     */   }
/*     */ }

/* Location:           C:\Users\Somar Ani\Downloads\parallax.jar
 * Qualified Name:     com.compani.shooter.entities.MovingPlatform
 * JD-Core Version:    0.6.2
 */